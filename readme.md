## Objets

Ce projet vise à :

- définir une configuration minimale pour commencer un projet GWT, version [2.12.1](https://www.gwtproject.org/release-notes.html#Release_Notes_2_12_1), sur Tomcat 9 et Java 17,
- définir l'outillage (Checkstyle, CPD, PMD, Spotbugs),
- tester quelques technologies (Hibernate, JWT, Leaflet, MVP, OpenAPI, REST, SOAP, SSE, websockets).

Ajouter et configurer ces paramètres dans le fichier `context.xml` de Tomcat :

```
	<Parameter name="gwtexpe.app.url" value="http://localhost:8080/gwtexpe" />
	<Parameter name="gwtexpe.b2access.client.id" value="gwtexpe" />
	<Parameter name="gwtexpe.b2access.client.secret" value="supersecret" />
	<Parameter name="gwtexpe.b2access.server" value="DEVELOPMENT" />
```

## Lancement en ligne de commandes

Modifier les valeurs de `src/main/tomcat9xconf/context.xml` pour utiliser B2Access.

- empaqueter : `mvn package`
- lancer le serveur sur Tomcat 9 embarqué : `mvn cargo:run -Plocal`
- ouvrir http://127.0.0.1:8080/gwtexpe/

Pour modifier en direct la partie cliente :

- lancer GWT Code Server : `mvn gwt:codeserver`

puis :

1. ouvrir http://127.0.0.1:9876/,
2. suivre les instructions,

## OAuth2

Cette application a été enregistrée sur le serveur de développement de B2Access à https://unity.eudat-aai.fz-juelich.de.

Sur ce serveur de développement B2Access :

- l'authentification par le CAS INRA n'est pas disponible,
- l'authentification par GitHub ne fonctionne pas,
- passer par ORCID permet d'utiliser le CAS INRA.

## Tests de JWT

1. Saisir son login `${USER}` dans [la page Hello](http://localhost:8080/gwtexpe/#HelloPlace:World!) pour créer un utilisateur.
2. Récupérer le JWT avec `JWT=$(curl --data "username=${USER}&password=${USER}" http://localhost:8080/gwtexpe/rs/auth/token)`.
3. Tester l'authentification `curl --header "Authorization: Bearer $JWT" http://localhost:8080/gwtexpe/rs/auth/info`.

## Problème de dépendance lors du lancement dans Eclipse

Parfois Eclipse perd les dépendances (Log4j non trouvé au lancement).
Clic droit sur **le projet**, choisir **Properties**, puis **Deployment Assembly** et vérifier que dans la colonne _Source_ il y a _Maven Dependencies_.
Si ce n'est pas le cas, cliquer sur **Add...**, **Java Build Path Entries** et **Maven Dependencies**, **Apply and close**.

## Avertissements de Tomcat

**Avertissement :**

    The background cache eviction process was unable to free [10] percent of the cache for context

**Solution :**

Rajouter dans `context.xml` cette configuration

    <Resources cachingAllowed="true" cacheMaxSize="100000" />
