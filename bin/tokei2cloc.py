#!/usr/bin/env python3
# -*- coding: UTF-8 -*-

# $Id: tokei2cloc.py 555 2021-11-04 15:44:55Z omaury $
#
# Author : Olivier Maury
# Creation Date : 2019-01-15 10:30:29 +0200
# Last Revision : $Date: 2021-11-04 16:44:55 +0100 (jeu. 04 nov. 2021) $
# Revision : $Rev: 555 $
u"""
[1mNOM[m
        %prog - Tokei2Cloc

[1mDESCRIPTION[m
        Formate la sortie de tokei dans le format de Cloc

[1mEXEMPLE[m
        tokei -f -o json bin src | %prog

[1mAUTEUR[m
        Olivier Maury

[1mVERSION[m
        $Date: 2021-11-04 16:44:55 +0100 (jeu. 04 nov. 2021) $
"""

__revision__ = "$Rev: 555 $"
__author__ = "Olivier Maury"
import json
import sys

results = json.loads(sys.stdin.read())

# header
nb_files = 0
nb_lines = 0
for lang in results:
    nb_files += len(results[lang]['stats'])
    nb_lines += int(results[lang]['lines'])

print("""<?xml version="1.0"?><results>
<header>
  <cloc_url>http://cloc.sourceforge.net</cloc_url>
  <cloc_version>1.60</cloc_version>
  <elapsed_seconds>0.348630905151367</elapsed_seconds>
  <n_files>%d</n_files>
  <n_lines>%d</n_lines>
  <files_per_second>510.568619619987</files_per_second>
  <lines_per_second>88902.0438005723</lines_per_second>
  <report_file>target/cloc.xml</report_file>
</header>
<files> """ % (nb_files, nb_lines))

# files
total_blank = 0
total_comment = 0
total_code = 0
for lang in results:
    for result in results[lang]['stats']:
        blank = int(result['blanks'])
        comment = int(result['comments'])
        code = int(result['code'])
        print("""  <file name="%s" blank="%d" comment="%d" code="%d"  language="%s" />""" % 
            (result['name'], blank, comment, code, lang))
        total_blank += blank
        total_comment += comment
        total_code += code

# total
print("""  <total blank="%d" comment="%d" code="%d" />
</files>
</results>""" % (total_blank, total_comment, total_code))
