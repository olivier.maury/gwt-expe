<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" session="false"%>
<!DOCTYPE html>
<html dir='ltr' lang='<%=request.getAttribute("lang")%>'
	xmlns='http://www.w3.org/TR/html5'>
<head>
<meta charset='UTF-8'>
<title>Upload file</title>
</head>
<body>
	<h1>Simple HTML form to upload files</h1>
	<form method="post" enctype="multipart/form-data" action="upload">
		<input type="file" name="file" multiple
			accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet,application/vnd.ms-excel">
		<br />
		<input type="submit" />
	</form>
</body>
</html>
