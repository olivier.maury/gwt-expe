<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Swagger UI</title>
    <link rel="stylesheet" type="text/css" href="webjars/swagger-ui/<%=request.getAttribute("swagger_version")%>/swagger-ui.css" />
    <script src="webjars/swagger-ui/<%=request.getAttribute("swagger_version")%>/swagger-ui-bundle.js"></script>
    <script src="webjars/swagger-ui/<%=request.getAttribute("swagger_version")%>/swagger-ui-standalone-preset.js"></script>
</head>
<body>
<div id="swagger-ui"></div>
<script>
    window.onload = function() {
    	const trimmed = window.location.href.replace("/swagger-ui", "");
    	const serverUrl = trimmed + "/api/";
        const ui = SwaggerUIBundle({
            url: trimmed + "/openapi/openapi.json",
            dom_id: '#swagger-ui',
            presets: [
                SwaggerUIBundle.presets.apis,
                SwaggerUIStandalonePreset
            ],
            layout: "BaseLayout",
            plugins : [
            	function() {
            		return {
            			statePlugins : {
            				spec: {
            					wrapActions: {
            						updateJsonSpec: function(oriAction, system) {
            							return (spec) => {
            								spec.servers = [{url: serverUrl}]
            			                    return oriAction(spec)
            							}
            						}
            					}
            				}
            			}
            		}
            	}
            ]
        });
    };
</script>
</body>
</html>
