package fr.inrae.agroclim.gwtexpe.client.goodbye;

/*-
 * #%L
 * GWT Experiments
 * %%
 * Copyright (C) 2018 - 2022 INRAE AgroClim
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.gwt.activity.shared.AbstractActivity;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.ui.AcceptsOneWidget;

import fr.inrae.agroclim.gwtexpe.client.ClientFactory;
import fr.inrae.agroclim.gwtexpe.client.hello.HelloPlace;

/**
 * Activity for Goodbye.
 */
public class GoodbyeActivity extends AbstractActivity
        implements GoodbyeView.Presenter {
    /**
     * Used to obtain views, eventBus, placeController. Alternatively, could be
     * injected via GIN.
     */
    private final ClientFactory clientFactory;

    /** User's name that will be appended to "Hello,". */
    private final String name;

    /**
     * Constructor.
     *
     * @param place
     *            related place
     * @param factory
     *            Factory to instantiate various components for the client.
     */
    public GoodbyeActivity(final GoodbyePlace place,
            final ClientFactory factory) {
        this.name = place.getName();
        this.clientFactory = factory;
    }

    @Override
    public final void start(final AcceptsOneWidget panel,
            final EventBus eventBus) {
        GoodbyeView view = clientFactory.getGoodbyeView();
        view.setName(name);
        view.setPresenter(this);
        panel.setWidget(view.asWidget());
        Timer timer = new Timer() {
            @Override
            public void run() {
                clientFactory.getPlaceController().goTo(new HelloPlace(name));
            }
        };
        final int delayMillis = 2_000;
        timer.schedule(delayMillis);
    }

}
