package fr.inrae.agroclim.gwtexpe.client;

/*-
 * #%L
 * GWT Experiments
 * %%
 * Copyright (C) 2018 - 2022 INRAE AgroClim
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.gwt.place.shared.Place;
import com.google.gwt.place.shared.PlaceHistoryHandler;
import com.google.gwt.place.shared.PlaceHistoryMapper;
import com.google.gwt.place.shared.WithTokenizers;

import fr.inrae.agroclim.gwtexpe.client.goodbye.GoodbyePlace;
import fr.inrae.agroclim.gwtexpe.client.gwtbootstrap3.Gwtbootstrap3Place;
import fr.inrae.agroclim.gwtexpe.client.hello.HelloPlace;
import fr.inrae.agroclim.gwtexpe.client.leaflet.LeafletClusterPlace;
import fr.inrae.agroclim.gwtexpe.client.leaflet.LeafletMapPlace;
import fr.inrae.agroclim.gwtexpe.client.ui.UiPlace;
import fr.inrae.agroclim.gwtexpe.client.upload.UploadPlace;
import fr.inrae.agroclim.gwtexpe.client.websocket.WebSocketPlace;

/**
 * Maps {@link Place}s to/from tokens, used to configure a
 * {@link PlaceHistoryHandler}.
 * <p>
 * Annotated with {@link WithTokenizers} to have their implementation
 * automatically generated via a call to
 * {@link com.google.gwt.core.shared.GWT#create(Class)}.
 */
@WithTokenizers({HelloPlace.Tokenizer.class, GoodbyePlace.Tokenizer.class, Gwtbootstrap3Place.Tokenizer.class,
    LeafletClusterPlace.Tokenizer.class, LeafletMapPlace.Tokenizer.class, UiPlace.Tokenizer.class,
    UploadPlace.Tokenizer.class, WebSocketPlace.Tokenizer.class})
public interface AppPlaceHistoryMapper extends PlaceHistoryMapper {
}
