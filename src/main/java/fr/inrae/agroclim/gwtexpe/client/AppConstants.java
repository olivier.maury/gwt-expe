package fr.inrae.agroclim.gwtexpe.client;

/*-
 * #%L
 * GWT Experiments
 * %%
 * Copyright (C) 2018 - 2022 INRAE AgroClim
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.gwt.i18n.client.Constants;

/**
 * I18n constants.
 */
public interface AppConstants extends Constants {
    /**
     * @return translation
     */
    @DefaultStringValue("An error occurred while attempting to contact the server. "
            + "Please check your network connection and try again.")
    String serverError();

    /**
     * @return translation
     */
    @DefaultStringValue("Remote Procedure Call - Failure")
    String serverErrorTitle();

    /**
     * @return translation
     */
    @DefaultStringValue("Remote Procedure Call - Success")
    String serverSuccessTitle();
}
