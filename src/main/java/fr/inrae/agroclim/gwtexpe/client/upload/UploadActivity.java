package fr.inrae.agroclim.gwtexpe.client.upload;

/*-
 * #%L
 * GWT Experiments
 * %%
 * Copyright (C) 2018 - 2022 INRAE AgroClim
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.gwt.activity.shared.AbstractActivity;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.user.client.ui.AcceptsOneWidget;

import fr.inrae.agroclim.gwtexpe.client.ClientFactory;

/**
 * Activity for upload form.
 */
public final class UploadActivity extends AbstractActivity implements UploadView.Presenter {
    /**
     * Used to obtain views, eventBus, placeController. Alternatively, could be
     * injected via GIN.
     */
    private final ClientFactory clientFactory;

    /**
     * Constructor.
     *
     * @param factory Factory to instantiate various components for the client.
     */
    public UploadActivity(final ClientFactory factory) {
        this.clientFactory = factory;
    }

    @Override
    public void start(final AcceptsOneWidget containerWidget, final EventBus eventBus) {
        final UploadView view = clientFactory.getUploadView();
        view.setPresenter(this);
        containerWidget.setWidget(view.asWidget());
    }

}
