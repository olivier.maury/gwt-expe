package fr.inrae.agroclim.gwtexpe.client.hello;

/*-
 * #%L
 * GWT Experiments
 * %%
 * Copyright (C) 2018 - 2022 INRAE AgroClim
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.gwt.activity.shared.AbstractActivity;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.AcceptsOneWidget;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.VerticalPanel;

import fr.inrae.agroclim.gwtexpe.client.ClientFactory;
import fr.inrae.agroclim.gwtexpe.client.GreetingService;
import fr.inrae.agroclim.gwtexpe.client.GreetingServiceAsync;
import fr.inrae.agroclim.gwtexpe.shared.FieldVerifier;

/**
 * Activity for Hello.
 */
public class HelloActivity extends AbstractActivity
implements HelloView.Presenter {

    /**
     * The message displayed to the user when the server cannot be reached or
     * returns an error.
     */
    private static final String SERVER_ERROR = "An error occurred while "
            + "attempting to contact the server. Please check your network "
            + "connection and try again.";

    /**
     * Used to obtain views, eventBus, placeController. Alternatively, could be
     * injected via GIN.
     */
    private final ClientFactory clientFactory;

    /**
     * Button to close dialog box.
     */
    private final Button closeButton = new Button("Close");

    /**
     * Dialog box to display server response.
     */
    private DialogBox dialogBox = null;

    /**
     * Create a remote service proxy to talk to the server-side Greeting
     * service.
     */
    private final GreetingServiceAsync greetingService = GWT
            .create(GreetingService.class);

    /** User's name that will be appended to "Hello,". */
    private final String name;

    /**
     * In dialog box to display server response.
     */
    private final HTML serverResponseLabel = new HTML();

    /**
     * In dialog box to display request text.
     */
    private final Label textToServerLabel = new Label();

    /**
     * Constructor.
     *
     * @param place
     *            related place
     * @param factory
     *            Factory to instantiate various components for the client.
     */
    public HelloActivity(final HelloPlace place, final ClientFactory factory) {
        this.name = place.getName();
        this.clientFactory = factory;
    }

    /**
     * Make dialog box to display request and server texts.
     */
    private void makeDialogBox() {
        if (dialogBox != null) {
            return;
        }
        // Create the popup dialog box
        dialogBox = new DialogBox();
        dialogBox.setText("Remote Procedure Call");
        dialogBox.setAnimationEnabled(true);
        // We can set the id of a widget by accessing its Element
        closeButton.getElement().setId("closeButton");
        final VerticalPanel dialogVPanel = new VerticalPanel();
        dialogVPanel.addStyleName("dialogVPanel");
        dialogVPanel.add(new HTML("<b>Sending name to the server:</b>"));
        dialogVPanel.add(textToServerLabel);
        dialogVPanel.add(new HTML("<br><b>Server replies:</b>"));
        dialogVPanel.add(serverResponseLabel);
        dialogVPanel.setHorizontalAlignment(VerticalPanel.ALIGN_RIGHT);
        dialogVPanel.add(closeButton);
        dialogBox.setWidget(dialogVPanel);

        // Add a handler to close the DialogBox
        closeButton.addClickHandler((final ClickEvent event) -> {
            dialogBox.hide();
            clientFactory.getHelloView().enableAndFocusSendButton();
        });
    }

    /**
     * Ask user before stopping this activity.
     *
     * Called when the user is trying to navigate away from this activity.
     *
     * @return A message to display to the user, e.g. to warn of unsaved work,
     *         or null to say nothing
     */
    @Override
    public final String mayStop() {
        return "Please hold on. This activity is stopping.";
    }

    @Override
    public final void send(final String text) {
        clientFactory.getHelloView().clearErrorLabel();

        if (!FieldVerifier.isValidName(text)) {
            clientFactory.getHelloView()
            .setErrorLabelText("Please enter at least four characters");
            return;
        }

        // Then, we send the input to the server.
        makeDialogBox();
        clientFactory.getHelloView().disableSendButton();
        textToServerLabel.setText(text);
        serverResponseLabel.setText("");
        greetingService.greetServer(text, new AsyncCallback<String>() {
            @Override
            public void onFailure(final Throwable caught) {
                // Show the RPC error message to the user
                dialogBox.setText("Remote Procedure Call - Failure");
                serverResponseLabel.addStyleName("serverResponseLabelError");
                serverResponseLabel.setHTML(SERVER_ERROR);
                dialogBox.center();
                closeButton.setFocus(true);
            }

            @Override
            public void onSuccess(final String result) {
                dialogBox.setText("Remote Procedure Call");
                serverResponseLabel.removeStyleName("serverResponseLabelError");
                serverResponseLabel.setHTML(result);
                dialogBox.center();
                closeButton.setFocus(true);
            }
        });
    }

    /**
     * Invoked by the ActivityManager to start a new Activity.
     *
     * Called when the Activity should ready its widget for the user. When the
     * widget is ready (typically after an RPC response has been received),
     * receiver should present it by calling {@link AcceptsOneWidget#setWidget}
     * on the given panel.
     * <p>
     * Any handlers attached to the provided event bus will be de-registered
     * when the activity is stopped, so activities will rarely need to hold on
     * to the {@link com.google.gwt.event.shared.HandlerRegistration
     * HandlerRegistration} instances returned by {@link EventBus#addHandler}.
     *
     * @param containerWidget
     *            the panel to display this activity's widget when it is ready
     * @param eventBus
     *            the event bus
     */
    @Override
    public final void start(final AcceptsOneWidget containerWidget,
            final EventBus eventBus) {
        final HelloView helloView = clientFactory.getHelloView();
        helloView.setName(name);
        helloView.setPresenter(this);
        containerWidget.setWidget(helloView.asWidget());
    }

}
