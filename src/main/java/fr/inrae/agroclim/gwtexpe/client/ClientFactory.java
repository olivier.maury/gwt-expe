package fr.inrae.agroclim.gwtexpe.client;

/*-
 * #%L
 * GWT Experiments
 * %%
 * Copyright (C) 2018 - 2022 INRAE AgroClim
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.gwt.place.shared.PlaceController;
import com.google.web.bindery.event.shared.EventBus;

import fr.inrae.agroclim.gwtexpe.client.goodbye.GoodbyeView;
import fr.inrae.agroclim.gwtexpe.client.gwtbootstrap3.Gwtbootstrap3View;
import fr.inrae.agroclim.gwtexpe.client.hello.HelloView;
import fr.inrae.agroclim.gwtexpe.client.leaflet.LeafletClusterView;
import fr.inrae.agroclim.gwtexpe.client.leaflet.LeafletMapView;
import fr.inrae.agroclim.gwtexpe.client.ui.UiView;
import fr.inrae.agroclim.gwtexpe.client.upload.UploadView;
import fr.inrae.agroclim.gwtexpe.client.websocket.WebSocketView;

/**
 * Factory to instantiate various components for the client.
 */
public interface ClientFactory {

    /**
     * @return Dispatcher of {@link com.google.web.bindery.event.shared.Event}s to
     *         interested parties
     */
    EventBus getEventBus();

    /**
     * @return view for Goodbye
     */
    GoodbyeView getGoodbyeView();

    /**
     * @return view for GwtBootstrap3 test page
     */
    Gwtbootstrap3View getGwtbootstrap3View();

    /**
     * @return view for Hello
     */
    HelloView getHelloView();

    /**
     * @return view for Leaflet test page to display points
     */
    LeafletMapView getLeafletMapView();

    /**
     * @return view for Leaflet test page
     */
    LeafletClusterView getLeafletView();

    /**
     * @return main page composite
     */
    MainPage getMainPage();

    /**
     * @return Controller in charge of the user's location in the app
     */
    PlaceController getPlaceController();

    /**
     * @return view for UI widget demo
     */
    UiView getUiView();

    /**
     * @return view for upload form
     */
    UploadView getUploadView();

    /**
     * @return view for WebSocket test page
     */
    WebSocketView getWebSocketView();

}
