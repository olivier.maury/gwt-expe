package fr.inrae.agroclim.gwtexpe.client.leaflet;

/*-
 * #%L
 * GWT Experiments
 * %%
 * Copyright (C) 2018 - 2022 INRAE AgroClim
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.peimari.gleaflet.client.GeoJSON;
import org.peimari.gleaflet.client.LatLng;
import org.peimari.gleaflet.client.Map;
import org.peimari.gleaflet.client.MapOptions;
import org.peimari.gleaflet.client.MapWidget;
import org.peimari.gleaflet.client.TooltipOptions;

import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.JavaScriptObject;
import com.google.gwt.core.client.JsArray;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Panel;
import com.google.gwt.user.client.ui.Widget;

/**
 * Implementation of LeafletView.
 *
 * @see <a href=
 *      "https://github.com/mstahv/gleafletexample/">mstahv/gleafletexample</a>,
 *      <a href="https://github.com/mstahv/g-leaflet/">mstahv/g-leaflet</a>,
 *      <a href=
 *      "https://github.com/octavm/g-leaflet-googlemutant/">octavm/g-leaflet-googlemutant</a>,
 *      <a href=
 *      "https://gitlab.com/IvanSanchez/Leaflet.GridLayer.GoogleMutant">IvanSanchez/Leaflet.GridLayer.GoogleMutant</a>,
 *      <a href=
 *      "https://github.com/mstahv/g-leaflet-draw">mstahv/g-leaflet-draw</a>,
 *      <a href=
 *      "https://github.com/Leaflet/Leaflet.draw">Leaflet/Leaflet.draw</a>.
 */
public final class LeafletMapViewImpl extends Composite implements LeafletMapView {

    /**
     * UI binder.
     */
    interface Binder extends UiBinder<Widget, LeafletMapViewImpl> {
    }

    /**
     * Longitude of map center.
     */
    private static final double CENTER_LONGITUDE = 4.82325;

    /**
     * Latitude of map center.
     */
    private static final double CENTER_LATITUDE = 43.933591;

    /**
     * Default zoom for map.
     */
    private static final double DEFAULT_ZOOM = 10;

    /**
     * UI binder.
     */
    private static final Binder UI_BINDER = GWT.create(Binder.class);

    /**
     * Leaflet map.
     */
    private Map map = null;

    /**
     * Panel containing the map.
     */
    @UiField
    protected Panel panel;

    /**
     * Leaflet GeoJSON.
     */
    private final GeoJSON featureGroup = GeoJSON.create();

    /**
     * Constructor.
     */
    public LeafletMapViewImpl() {
        initWidget(UI_BINDER.createAndBindUi(this));
    }

    public native void bindTooltip(GeoJSON featureGp,
            TooltipOptions tooltipOptions)
    /*-{
        featureGp.bindTooltip(function(layer) {
            //merely sets the tooltip text
            return "maille #" + layer.feature.id;
        }, tooltipOptions);
    }-*/;

    /**
     * Create only once the map.
     */
    private void createMap() {
        if (map != null) {
            return;
        }
        final MapOptions options = MapOptions.create();
        final MapWidget widget = new MapWidget(options);
        panel.add(widget);

        map = widget.getMap();
        map.setView(LatLng.create(CENTER_LATITUDE, CENTER_LONGITUDE), DEFAULT_ZOOM);

        map.addControl(LeafletUtils.createLayersControl(map));

        map.addLayer(featureGroup);

    }

    @Override
    public void draw(final JsArray<JavaScriptObject> features) {
        featureGroup.clearLayers();
        featureGroup.addData(features);
        final TooltipOptions tooltipOptions = TooltipOptions.create();
        // https://leafletjs.com/examples/geojson/
        // https://leafletjs.com/reference-1.6.0.html#geojson
        bindTooltip(featureGroup, tooltipOptions);
    }

    @Override
    protected void onAttach() {
        super.onAttach();
        createMap();
    }

    @Override
    public void setPresenter(final Presenter p) {
    }

}
