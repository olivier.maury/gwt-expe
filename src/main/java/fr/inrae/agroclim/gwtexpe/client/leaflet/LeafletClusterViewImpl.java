package fr.inrae.agroclim.gwtexpe.client.leaflet;

/*-
 * #%L
 * GWT Experiments
 * %%
 * Copyright (C) 2018 - 2022 INRAE AgroClim
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.peimari.gleaflet.client.Circle;
import org.peimari.gleaflet.client.CircleMarker;
import org.peimari.gleaflet.client.CircleMarkerOptions;
import org.peimari.gleaflet.client.GeoJSON;
import org.peimari.gleaflet.client.LatLng;
import org.peimari.gleaflet.client.Map;
import org.peimari.gleaflet.client.MapOptions;
import org.peimari.gleaflet.client.MapWidget;
import org.peimari.gleaflet.client.Marker;
import org.peimari.gleaflet.client.MarkerOptions;
import org.peimari.gleaflet.client.draw.Draw;
import org.peimari.gleaflet.client.draw.DrawControlOptions;
import org.vaadin.gleaflet.markercluster.client.MarkerClusterGroupOptions;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Panel;
import com.google.gwt.user.client.ui.Widget;

/**
 * Implementation of LeafletView.
 *
 * @see <a href=
 *      "https://github.com/mstahv/gleafletexample/">mstahv/gleafletexample</a>,
 *      <a href="https://github.com/mstahv/g-leaflet/">mstahv/g-leaflet</a>,
 *      <a href=
 *      "https://github.com/octavm/g-leaflet-googlemutant/">octavm/g-leaflet-googlemutant</a>,
 *      <a href=
 *      "https://gitlab.com/IvanSanchez/Leaflet.GridLayer.GoogleMutant">IvanSanchez/Leaflet.GridLayer.GoogleMutant</a>,
 *      <a href=
 *      "https://github.com/Leaflet/Leaflet.markercluster">Leaflet/Leaflet.markercluster</a>,
 *      <a href=
 *      "https://github.com/mstahv/g-leaflet-draw">mstahv/g-leaflet-draw</a>,
 *      <a href=
 *      "https://github.com/Leaflet/Leaflet.draw">Leaflet/Leaflet.draw</a>.
 */
public final class LeafletClusterViewImpl extends Composite
implements LeafletClusterView {

    /**
     * UI binder.
     */
    interface Binder extends UiBinder<Widget, LeafletClusterViewImpl> {
    }

    /**
     * UI binder.
     */
    private static final Binder UI_BINDER = GWT.create(Binder.class);

    /**
     * Leaflet map.
     */
    private Map map = null;

    /**
     * Panel containing the map.
     */
    @UiField
    protected Panel panel;

    /**
     * Constructor.
     */
    public LeafletClusterViewImpl() {
        initWidget(UI_BINDER.createAndBindUi(this));
    }

    /**
     * Create only once the map.
     */
    private void createMap() {
        if (map != null) {
            return;
        }
        final MapOptions options = MapOptions.create();
        final MapWidget widget = new MapWidget(options);
        panel.add(widget);

        map = widget.getMap();
        map.setView(LatLng.create(43.933591, 4.82325), (double) 10);

        map.addControl(LeafletUtils.createLayersControl(map));

        final GeoJSON featureGroup = GeoJSON.create();

        final CircleMarkerOptions circleMarkerOptions = CircleMarkerOptions.create();
        circleMarkerOptions.setColor("red");
        final CircleMarker circleMarker = CircleMarker.create(LatLng.create(44, 5),
                circleMarkerOptions);
        featureGroup.addLayer(circleMarker);

        final CircleMarkerOptions pathOptions = CircleMarkerOptions.create();
        pathOptions.setColor("green");
        final Circle circle = Circle.create(LatLng.create(44, 5.1), 500d,
                pathOptions);
        featureGroup.addLayer(circle);

        map.addLayer(featureGroup);

        final MarkerClusterGroupOptions clusterOptions = MarkerClusterGroupOptions.create();
        final MarkerClusterGroup clusterGroup = MarkerClusterGroup.create(clusterOptions);
        final MarkerOptions markerOptions = MarkerOptions.create();
        final double step = 0.1;
        final double startLat = 44.;
        final double endLat = 45.;
        for (double lat = startLat; lat < endLat; lat += step) {
            final double startLon = 5.;
            final double endLon = 6.;
            for (double lon = startLon; lon < endLon; lon += step) {
                final Marker marker = Marker.create(LatLng.create(lat, lon), markerOptions);
                marker.bindPopup("[" + lat + "; " + lon + "]");
                clusterGroup.addLayer(marker);
            }
        }
        map.addLayer(clusterGroup);

        final DrawControlOptions drawOptions = DrawControlOptions.create();
        drawOptions.setEditableFeatureGroup(featureGroup);
        final Draw drawControl = Draw.create(drawOptions);
        map.addControl(drawControl);

    }

    @Override
    protected void onAttach() {
        super.onAttach();
        createMap();
    }

    @Override
    public void setPresenter(final Presenter p) {
    }

}
