package fr.inrae.agroclim.gwtexpe.client.leaflet;

/*-
 * #%L
 * GWT Experiments
 * %%
 * Copyright (C) 2018 - 2022 INRAE AgroClim
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.gwt.activity.shared.AbstractActivity;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.user.client.ui.AcceptsOneWidget;

import fr.inrae.agroclim.gwtexpe.client.ClientFactory;

/**
 * Activity for Leaflet test page.
 */
public class LeafletClusterActivity extends AbstractActivity
implements LeafletClusterView.Presenter {

    /**
     * Used to obtain views, eventBus, placeController.
     *
     * Alternatively, could be injected via GIN.
     */
    private final ClientFactory clientFactory;

    /**
     * Persisted view.
     */
    private LeafletClusterView view;

    /**
     * Constructor.
     *
     * @param factory
     *            Factory to instantiate various components for the client.
     */
    public LeafletClusterActivity(final ClientFactory factory) {
        this.clientFactory = factory;
    }

    /**
     * Invoked by the ActivityManager to start a new Activity.
     *
     * Called when the Activity should ready its widget for the user. When the
     * widget is ready (typically after an RPC response has been received),
     * receiver should present it by calling {@link AcceptsOneWidget#setWidget}
     * on the given panel.
     * <p>
     * Any handlers attached to the provided event bus will be de-registered
     * when the activity is stopped, so activities will rarely need to hold on
     * to the {@link com.google.gwt.event.shared.HandlerRegistration
     * HandlerRegistration} instances returned by {@link EventBus#addHandler}.
     *
     * @param containerWidget
     *            the panel to display this activity's widget when it is ready
     * @param eventBus
     *            the event bus
     */
    @Override
    public final void start(final AcceptsOneWidget containerWidget,
            final EventBus eventBus) {
        view = clientFactory.getLeafletView();
        view.setPresenter(this);
        containerWidget.setWidget(view.asWidget());
    }

}
