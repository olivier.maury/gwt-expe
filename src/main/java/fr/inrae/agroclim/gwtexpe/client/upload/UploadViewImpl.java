package fr.inrae.agroclim.gwtexpe.client.upload;

/*-
 * #%L
 * GWT Experiments
 * %%
 * Copyright (C) 2018 - 2022 INRAE AgroClim
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.StringJoiner;

import org.gwtbootstrap3.client.ui.Button;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FileUpload;
import com.google.gwt.user.client.ui.FormPanel;
import com.google.gwt.user.client.ui.Widget;

/**
 * Implementation of {@link UploadView}.
 */
public final class UploadViewImpl extends Composite implements UploadView {
    /**
     * UI binder.
     */
    interface Binder extends UiBinder<Widget, UploadViewImpl> {
    }

    /**
     * UI binder.
     */
    private static final Binder UI_BINDER = GWT.create(Binder.class);

    /**
     * Form HTML element.
     */
    @UiField
    protected FormPanel formPanel;

    /**
     * Input HTML widget.
     */
    @UiField
    protected FileUpload fileUpload;

    /**
     * Button to trigger POST request.
     */
    @UiField
    protected Button sendButton;

    /**
     * Constructor.
     */
    public UploadViewImpl() {
        initWidget(UI_BINDER.createAndBindUi(this));

        formPanel.setAction(GWT.getHostPageBaseURL() + "upload");
        fileUpload.getElement().setPropertyString("multiple", "multiple");
        final StringJoiner sj = new StringJoiner(",");
        // .xlsx
        sj.add("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        // .xls
        sj.add("application/vnd.ms-excel");
        fileUpload.getElement().setAttribute("accept", sj.toString());

        formPanel.addSubmitCompleteHandler(ev -> Window.alert("Submit is complete" + ev.getResults()));
    }

    @UiHandler("fileUpload")
    public void onFileChange(final ChangeEvent event) {
        sendButton.setEnabled(fileUpload.getFilename() != null && !"".equals(fileUpload.getFilename()));
    }

    @UiHandler("sendButton")
    public void onSendClick(final ClickEvent event) {
        GWT.log("UploadViewImpl.onSendClick()");
        formPanel.submit();
    }

    @Override
    public void setPresenter(final Presenter presenter) {
    }

}
