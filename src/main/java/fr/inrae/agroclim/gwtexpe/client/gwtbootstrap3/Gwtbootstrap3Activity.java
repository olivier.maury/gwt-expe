package fr.inrae.agroclim.gwtexpe.client.gwtbootstrap3;

/*-
 * #%L
 * GWT Experiments
 * %%
 * Copyright (C) 2018 - 2022 INRAE AgroClim
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.gwt.activity.shared.AbstractActivity;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.AcceptsOneWidget;

import fr.inrae.agroclim.gwtexpe.client.AppConstants;
import fr.inrae.agroclim.gwtexpe.client.ClientFactory;
import fr.inrae.agroclim.gwtexpe.client.GreetingService;
import fr.inrae.agroclim.gwtexpe.client.GreetingServiceAsync;

/**
 * Activity for GwtBoostrap3.
 */
public final class Gwtbootstrap3Activity extends AbstractActivity implements Gwtbootstrap3View.Presenter {
    /**
     * I18n constants.
     */
    private static final AppConstants CSTS = GWT.create(AppConstants.class);

    /**
     * Used to obtain views, eventBus, placeController. Alternatively, could be
     * injected via GIN.
     */
    private final ClientFactory clientFactory;

    /**
     * ser's name that will be appended to "Hello,".
     */
    private final String name;

    /**
     * Create a remote service proxy to talk to the server-side Greeting
     * service.
     */
    private final GreetingServiceAsync greetingService = GWT.create(GreetingService.class);

    /**
     * Constructor.
     *
     * @param place   related place
     * @param factory Factory to instantiate various components for the client.
     */
    public Gwtbootstrap3Activity(final Gwtbootstrap3Place place, final ClientFactory factory) {
        this.name = place.getName();
        this.clientFactory = factory;
    }

    @Override
    public void send(final String text) {
        greetingService.greetServer(text, new AsyncCallback<String>() {
            @Override
            public void onFailure(final Throwable caught) {
                final ModalDialog dialog = new ModalDialog();
                dialog.setTitle(CSTS.serverErrorTitle());
                dialog.setContent(CSTS.serverError() + " : " + caught.getMessage());
                dialog.show();
            }

            @Override
            public void onSuccess(final String result) {
                final ModalDialog dialog = new ModalDialog();
                dialog.setTitle(CSTS.serverSuccessTitle());
                dialog.setContent(result);
                dialog.show();
            }
        });
    }

    @Override
    public void start(final AcceptsOneWidget panel, final EventBus eventBus) {
        final Gwtbootstrap3View view = clientFactory.getGwtbootstrap3View();
        view.setName(name);
        view.setPresenter(this);
        panel.setWidget(view.asWidget());
    }

}
