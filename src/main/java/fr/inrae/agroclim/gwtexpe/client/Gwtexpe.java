package fr.inrae.agroclim.gwtexpe.client;

/*-
 * #%L
 * GWT Experiments
 * %%
 * Copyright (C) 2018 - 2022 INRAE AgroClim
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.gwt.activity.shared.ActivityManager;
import com.google.gwt.activity.shared.ActivityMapper;
import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Document;
import com.google.gwt.dom.client.Element;
import com.google.gwt.dom.client.ScriptElement;
import com.google.gwt.place.shared.Place;
import com.google.gwt.place.shared.PlaceController;
import com.google.gwt.place.shared.PlaceHistoryHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.web.bindery.event.shared.EventBus;
import fr.inrae.agroclim.gwtexpe.client.hello.HelloPlace;
import fr.inrae.agroclim.gwtexpe.shared.PersonDTO;
import org.cleanlogic.sse.EventSource;
import org.peimari.gleaflet.client.resources.LeafletDrawResourceInjector;
import org.peimari.gleaflet.client.resources.LeafletResourceInjector;

/**
 * Entry point classes define <code>onModuleLoad()</code>.
 */
public class Gwtexpe implements EntryPoint {

    /**
     * Default place to show.
     */
    private Place defaultPlace;

    /**
     * Create a remote service proxy to talk to the server-side Greeting service.
     */
    private final GreetingServiceAsync greetingService = GWT.create(GreetingService.class);

    private void injectGMapsUrl() {
        final Element head = Document.get().getElementsByTagName("head").getItem(0);
        final ScriptElement sce = Document.get().createScriptElement();
        sce.setType("text/javascript");
        sce.setSrc("https://maps.googleapis.com/maps/api/js?key=AIzaSyDLNYxy2u-P19lxLKI5s33dTW4URH2Jlnw");
        head.appendChild(sce);
    }

    /**
     * This is the entry point method.
     */
    @Override
    public final void onModuleLoad() {
        LeafletResourceInjector.ensureInjected();
        LeafletDrawResourceInjector.ensureInjected();
        injectGMapsUrl();

        greetingService.userName(new AsyncCallback<String>() {
            @Override
            public void onFailure(final Throwable caught) {
                defaultPlace = new HelloPlace("World!");
                onModuleLoadNext();
            }

            @Override
            public void onSuccess(final String userName) {
                defaultPlace = new HelloPlace(userName);
                onModuleLoadNext();
            }
        });
    }

    private void onModuleLoadNext() {
        // Create ClientFactory using deferred binding so we can replace with
        // different
        // impls in gwt.xml
        final ClientFactory clientFactory = GWT.create(ClientFactory.class);
        final EventBus eventBus = clientFactory.getEventBus();
        final PlaceController placeController = clientFactory.getPlaceController();

        final MainPage mainPage = clientFactory.getMainPage();

        // Start ActivityManager for the main widget with our ActivityMapper
        final ActivityMapper activityMapper = new AppActivityMapper(clientFactory);
        final ActivityManager activityManager = new ActivityManager(activityMapper, eventBus);
        activityManager.setDisplay(mainPage.getPanel());

        // Start PlaceHistoryHandler with our PlaceHistoryMapper
        final AppPlaceHistoryMapper historyMapper = GWT.create(AppPlaceHistoryMapper.class);
        final PlaceHistoryHandler historyHandler = new PlaceHistoryHandler(historyMapper);
        // Set the default place
        historyHandler.register(placeController, eventBus, defaultPlace);

        RootPanel.get().add(mainPage);
        // Goes to place represented on URL or default place
        historyHandler.handleCurrentHistory();

        // Server-Send Event
        final EventSource sse = new EventSource("/gwtexpe/rs/sse/persons");
        sse.onOpen = (final org.cleanlogic.sse.Event event) -> {
            mainPage.displayLogMessage("sse/persons Open readyState: " + event.target.readyState());
            mainPage.displayLogMessage("sse/persons Open lastEventId: " + event.lastEventId);
        };
        sse.onError = (final org.cleanlogic.sse.Event event) -> {
            mainPage.displayLogMessage("sse/persons Error: " + event.target.readyState());
        };
        sse.onMessage = (final org.cleanlogic.sse.Event event) -> {
            mainPage.displayLogMessage("sse/persons data: " + event.data);
        };
        // Custom listener
        sse.addEventListener("person", (final org.cleanlogic.sse.Event event) -> {
            final PersonDTO dto = JSON.parse(event.data);
            mainPage.displayLogMessage(
                    "sse/persons personDTO: " + dto.id + ", " + dto.name + ", " + dto.getCreatedDate());
        });
        // Server-Send Event broadcast
        // Launch with http://localhost:8080/gwtexpe/rs/sse/publish
        final EventSource broadcastSse = new EventSource("/gwtexpe/rs/sse/subscribe");
        broadcastSse.onOpen = (final org.cleanlogic.sse.Event event) -> {
            mainPage.displayLogMessage("sse/subscribe Open readyState: " + event.target.readyState());
            mainPage.displayLogMessage("sse/subscribe Open lastEventId: " + event.lastEventId);
        };
        broadcastSse.onError = (final org.cleanlogic.sse.Event event) -> {
            mainPage.displayLogMessage("sse/subscribe Error: " + event.target.readyState());
        };
        broadcastSse.onMessage = (final org.cleanlogic.sse.Event event) -> {
            mainPage.displayLogMessage("sse/subscribe subscribed data: " + event.data);
        };
        // Custom listener
        broadcastSse.addEventListener("message", (final org.cleanlogic.sse.Event event) -> {
            mainPage.displayLogMessage("sse/subscribe subscribed message: " + event.data);
        });
    }
}
