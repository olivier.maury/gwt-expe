package fr.inrae.agroclim.gwtexpe.client.gwtbootstrap3;

/*-
 * #%L
 * GWT Experiments
 * %%
 * Copyright (C) 2018 - 2022 INRAE AgroClim
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.gwtbootstrap3.client.ui.Modal;
import org.gwtbootstrap3.client.ui.ModalBody;

import com.google.gwt.user.client.ui.HTML;

/**
 * Modal dialog to display server response.
 */
public final class ModalDialog {
    /**
     * Body of modal.
     */
    private final ModalBody body = new ModalBody();

    /**
     * Embedded modal.
     */
    private final Modal modal = new Modal();

    /**
     * Constructor.
     */
    public ModalDialog() {
        modal.add(body);
        // ESC key to close the dialog.
        modal.setDataKeyboard(true);
    }

    /**
     * @param text text to display
     */
    public void setContent(final String text) {
        final HTML html = new HTML(text);
        body.clear();
        body.add(html);
    }

    /**
     * Sets the modal title.
     *
     * @param title the new title
     */
    public void setTitle(final String title) {
        modal.setTitle(title);
    }

    /**
     * Show the modal.
     */
    public void show() {
        modal.show();
    }

}
