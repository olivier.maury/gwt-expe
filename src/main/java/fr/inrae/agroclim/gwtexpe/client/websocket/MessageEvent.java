package fr.inrae.agroclim.gwtexpe.client.websocket;

/*-
 * #%L
 * GWT Experiments
 * %%
 * Copyright (C) 2018 - 2022 INRAE AgroClim
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.gwt.core.client.JavaScriptObject;

/**
 * .cast()'ed event of Function WebSocket listener.
 */
public class MessageEvent extends JavaScriptObject {

    /**
     * Constructor.
     */
    protected MessageEvent() {

    }

    /**
     * @return The data sent by the message emitter.
     */
    public final native String getData()/*-{
        return this.data;
    }-*/;

    /**
     * @return A USVString representing the origin of the message emitter.
     */
    public final native String getOrigin()/*-{
        return this.origin;
    }-*/;

}
