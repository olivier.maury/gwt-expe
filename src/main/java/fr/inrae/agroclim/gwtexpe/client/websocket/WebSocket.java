package fr.inrae.agroclim.gwtexpe.client.websocket;

/*-
 * #%L
 * GWT Experiments
 * %%
 * Copyright (C) 2018 - 2022 INRAE AgroClim
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import jsinterop.annotations.JsConstructor;
import jsinterop.annotations.JsMethod;
import jsinterop.annotations.JsPackage;
import jsinterop.annotations.JsProperty;
import jsinterop.annotations.JsType;

/**
 * The WebSocket object provides the API for creating and managing a WebSocket
 * connection to a server, as well as for sending and receiving data on the
 * connection.
 */
@JsType(isNative = true, namespace = JsPackage.GLOBAL)
public class WebSocket extends EventTarget {
    /**
     * An event listener to be called when the WebSocket connection's readyState
     * changes to CLOSED. The listener receives a CloseEvent named "close".
     */
    @JsProperty
    public Function onclose;

    /**
     * An event listener to be called when an error occurs. This is a simple
     * event named "error".
     */
    @JsProperty
    public Function onerror;

    /**
     * An event listener to be called when a message is received from the
     * server. The listener receives a MessageEvent named "message".
     */
    @JsProperty
    public Function onmessage;

    /**
     * An event listener to be called when the WebSocket connection's readyState
     * changes to OPEN; this indicates that the connection is ready to send and
     * receive data. The event is a simple one with the name "open".
     */
    @JsProperty
    public Function onopen;

    /**
     * The URL to which to connect; this should be the URL to which the
     * WebSocket server will respond.
     */
    @JsProperty
    public String url;

    /**
     * @param wsUrl
     *            The URL to which to connect; this should be the URL to which
     *            the WebSocket server will respond.
     */
    @JsConstructor
    public WebSocket(final String wsUrl) {

    }

    /**
     * Closes the WebSocket connection or connection attempt, if any. If the
     * connection is already CLOSED, this method does nothing.
     */
    @JsMethod
    public native void close();

    /**
     * Enqueues the specified data to be transmitted to the server over the
     * WebSocket connection, increasing the value of bufferedAmount by the
     * number of bytes needed to contain the data. If the data can't be sent
     * (for example, because it needs to be buffered but the buffer is full),
     * the socket is closed automatically.
     *
     * @param data
     *            The data to send to the server.
     */
    @JsMethod
    public native void send(String data);
}
