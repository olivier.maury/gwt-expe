package fr.inrae.agroclim.gwtexpe.client.websocket;

/*-
 * #%L
 * GWT Experiments
 * %%
 * Copyright (C) 2018 - 2022 INRAE AgroClim
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;

/**
 * Implementation of WebSocketView.
 */
public class WebSocketViewImpl extends Composite implements WebSocketView {

    /**
     * UI binder.
     */
    interface WebSocketViewImplUiBinder
            extends UiBinder<Widget, WebSocketViewImpl> {
    }

    /**
     * UI binder.
     */
    private static final WebSocketViewImplUiBinder UI_BINDER = GWT
            .create(WebSocketViewImplUiBinder.class);

    /**
     * Panel to add WebSocket messages.
     */
    @UiField
    VerticalPanel panel;

    /**
     * Presenter of WebSocket.
     */
    private Presenter presenter;

    /**
     * Input to send text to websocket server.
     */
    @UiField
    TextBox textBox;

    /**
     * Constructor.
     */
    public WebSocketViewImpl() {
        initWidget(UI_BINDER.createAndBindUi(this));
    }

    /**
     * Handling button click.
     *
     * @param e
     *            event not used
     */
    @UiHandler("button")
    final void onClickSend(final ClickEvent e) {
        presenter.send(textBox.getText());
    }

    @Override
    public final void setPresenter(final Presenter p) {
        this.presenter = p;
    }

    @Override
    public final void showError(final String msg) {
        Label label = new Label(msg);
        label.getElement().getStyle().setColor("red");
        panel.add(label);
    }

    @Override
    public final void showText(final String msg) {
        Label label = new Label(msg);
        label.getElement().getStyle().setColor("blue");
        panel.add(label);
    }

}
