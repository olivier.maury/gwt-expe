package fr.inrae.agroclim.gwtexpe.client;

/*-
 * #%L
 * GWT Experiments
 * %%
 * Copyright (C) 2018 - 2022 INRAE AgroClim
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.Scheduler.ScheduledCommand;
import com.google.gwt.place.shared.PlaceChangeEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.Window.Location;
import com.google.gwt.user.client.ui.AcceptsOneWidget;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.Widget;

import fr.inrae.agroclim.gwtexpe.client.goodbye.GoodbyePlace;
import fr.inrae.agroclim.gwtexpe.client.gwtbootstrap3.Gwtbootstrap3Place;
import fr.inrae.agroclim.gwtexpe.client.hello.HelloPlace;
import fr.inrae.agroclim.gwtexpe.client.leaflet.LeafletClusterPlace;
import fr.inrae.agroclim.gwtexpe.client.leaflet.LeafletMapPlace;
import fr.inrae.agroclim.gwtexpe.client.ui.Menu;
import fr.inrae.agroclim.gwtexpe.client.ui.UiPlace;
import fr.inrae.agroclim.gwtexpe.client.upload.UploadPlace;
import fr.inrae.agroclim.gwtexpe.client.websocket.WebSocketPlace;

/**
 * Implementation of main page composite.
 */
public final class MainPageImpl extends MainPage {
    /**
     * UI binder.
     */
    interface Binder extends UiBinder<Widget, MainPageImpl> {
    }

    /**
     * UI binder.
     */
    private static final Binder UI_BINDER = GWT.create(Binder.class);

    /**
     * Factory to instantiate various components for the client.
     */
    private final ClientFactory clientFactory;

    /**
     * Panel to display inner views.
     */
    @UiField
    protected FlowPanel footer;

    /**
     * Application menu bar.
     */
    @UiField
    protected Menu menu;

    /**
     * Panel to display inner views.
     */
    @UiField
    protected SimplePanel panel;

    /**
     * Constructor.
     *
     * @param factory
     *            Factory to instantiate various components for the client.
     */
    public MainPageImpl(final ClientFactory factory) {
        initWidget(UI_BINDER.createAndBindUi(this));
        this.clientFactory = factory;
        buildMenuBar();
    }

    /**
     * Build application menu bar.
     */
    private void buildMenuBar() {
        menu.addHome("Hello", new HelloPlace("World!"));
        menu.addItem("GoodBye", new GoodbyePlace("World!"));
        menu.addSubItem("GoodBye", "Ĝis", new GoodbyePlace("Ĝis!"));
        menu.addSubItem("GoodBye", "Ĝis la revido", new GoodbyePlace("Ĝis la revido!"));
        menu.addItem("WebSocket", new WebSocketPlace(""));
        menu.addItem("REST", (ScheduledCommand) null);
        menu.addSubItem("REST", "JAX-RS",
                () -> Window.open("rs/zakee-service/who", "_blank", ""));
        menu.addSubItem("REST", "OpenAPI",
                () -> Window.open("openapi/openapi.json", "_blank", ""));
        menu.addSubItem("REST", "Swagger UI", () -> Window.open("swagger-ui", "_blank", ""));
        menu.addItem("OAuth2 login", () -> {
            final String base = Location.getPath();
            Window.Location.replace(base + "/oauth2/login");
        });
        menu.addItem("UI library", (ScheduledCommand) null);
        menu.addSubItem("UI library", "Leaflet clusters", new LeafletClusterPlace(""));
        menu.addSubItem("UI library", "Leaflet points", new LeafletMapPlace(""));
        menu.addSubItem("UI library", "GwtBootstrap3", new Gwtbootstrap3Place("World!"));
        menu.addSubItem("UI library", "UI widgets", new UiPlace(""));
        menu.addSubItem("UI library", "File upload", new UploadPlace());

        clientFactory.getEventBus().addHandler(PlaceChangeEvent.TYPE, menu);
    }

    @Override
    public void displayLogMessage(final String message) {
        GWT.log("log message = " + message);
        final HTML html = new HTML();
        html.setText(message);
        footer.add(html);
        final int max = 5;
        if (footer.getWidgetCount() > max) {
            footer.remove(0);
        }
    }

    @Override
    public AcceptsOneWidget getPanel() {
        return panel;
    }
}
