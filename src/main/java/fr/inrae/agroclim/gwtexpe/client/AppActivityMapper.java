package fr.inrae.agroclim.gwtexpe.client;

/*-
 * #%L
 * GWT Experiments
 * %%
 * Copyright (C) 2018 - 2022 INRAE AgroClim
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.gwt.activity.shared.Activity;
import com.google.gwt.activity.shared.ActivityManager;
import com.google.gwt.activity.shared.ActivityMapper;
import com.google.gwt.place.shared.Place;

/**
 * Finds the activity to run for a given {@link Place}, used to configure an
 * {@link ActivityManager}.
 */
public class AppActivityMapper implements ActivityMapper {
    /**
     * Factory to instantiate various components for the client.
     */
    private final ClientFactory clientFactory;

    /**
     * Constructor.
     *
     * @param factory
     *            Factory to instantiate various components for the client.
     */
    public AppActivityMapper(final ClientFactory factory) {
        super();
        this.clientFactory = factory;
    }

    @Override
    public final Activity getActivity(final Place place) {
        if (place instanceof HasActivityPlace) {
            return ((HasActivityPlace) place).getActivity(clientFactory);
        } else {
            throw new UnsupportedOperationException("Please, implement HasActivityPlace for the place "
                    + place.toString() + ".");
        }
    }
}
