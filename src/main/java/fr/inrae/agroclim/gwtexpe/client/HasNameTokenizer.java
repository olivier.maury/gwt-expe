package fr.inrae.agroclim.gwtexpe.client;

/*-
 * #%L
 * GWT Experiments
 * %%
 * Copyright (C) 2018 - 2022 INRAE AgroClim
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.gwt.place.shared.PlaceTokenizer;
import java.util.function.Function;

/**
 * PlaceTokenizer for HasNamePlace.
 *
 * @param <T>
 *            HasNamePlace class
 */
public class HasNameTokenizer<T extends HasNamePlace>
        implements PlaceTokenizer<T> {

    /**
     * Instantiate the place from the token.
     */
    private final Function<String, T> placeGenerator;

    /**
     * Constructor.
     *
     * @param generator
     *            place generator
     */
    public HasNameTokenizer(final Function<String, T> generator) {
        placeGenerator = generator;
    }

    @Override
    public final T getPlace(final String token) {
        return placeGenerator.apply(token);
    }

    @Override
    public final String getToken(final T place) {
        return place.getName();
    }

}
