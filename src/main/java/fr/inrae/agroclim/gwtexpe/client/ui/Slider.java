package fr.inrae.agroclim.gwtexpe.client.ui;

/*-
 * #%L
 * GWT Experiments
 * %%
 * Copyright (C) 2018 - 2022 INRAE AgroClim
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Document;
import com.google.gwt.dom.client.InputElement;
import com.google.gwt.dom.client.StyleInjector;
import com.google.gwt.resources.client.ClientBundle;
import com.google.gwt.resources.client.TextResource;
import com.google.gwt.user.client.ui.Widget;

public final class Slider extends Widget {
    // TODO : return engine (gecko, webkit...) instead of browser name
    public static native String browserEngine()
    /*-{
        var tem;
        var ua = navigator.userAgent;
        var M = ua
                .match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i)
                || [];
        if (/trident/i.test(M[1])) {
            tem = /\brv[ :]+(\d+)/g.exec(ua) || [];
            return 'IE ' + (tem[1] || '');
        }
        if (M[1] === 'Chrome') {
            tem = ua.match(/\bOPR\/(\d+)/)
            if (tem != null) {
                return 'Opera ' + tem[1];
            }
        }
        M = M[2] ? [ M[1], M[2] ] : [ navigator.appName, navigator.appVersion,
                '-?' ];
        if ((tem = ua.match(/version\/(\d+)/i)) != null) {
            M.splice(1, 1, tem[1]);
        }
        return M[0].toLowerCase();
    }-*/;

    interface Resource extends ClientBundle {
        /**
         * Instance.
         */
        Resource INSTANCE = GWT.create(Resource.class);
        /**
         * @return CSS
         */
        @Source("slider.css")
        TextResource style();
    }

    /**
     * HTML element: "input".
     */
    private final InputElement input;

    /**
     * Constructor.
     */
    public Slider() {
        setElement(Document.get().createDivElement());
        input = Document.get().createTextInputElement();
        input.setAttribute("type", "range");
        input.addClassName(Slider.browserEngine());
        getElement().appendChild(input);
        StyleInjector.injectAtEnd(Resource.INSTANCE.style().getText());
        // TODO oninput event to update p
    }

    public void setMax(final String value) {
        input.setAttribute("max", value);
    }

    public void setMin(final String value) {
        input.setAttribute("min", value);
    }

    public void setStep(final String value) {
        input.setAttribute("step", value);
    }
}
