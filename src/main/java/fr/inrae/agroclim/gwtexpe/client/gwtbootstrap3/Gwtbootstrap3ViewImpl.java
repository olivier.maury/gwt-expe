package fr.inrae.agroclim.gwtexpe.client.gwtbootstrap3;

/*-
 * #%L
 * GWT Experiments
 * %%
 * Copyright (C) 2018 - 2022 INRAE AgroClim
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.gwtbootstrap3.client.GwtBootstrap3EntryPoint;
import org.gwtbootstrap3.client.ui.Button;
import org.gwtbootstrap3.client.ui.TextBox;

/*-
 * #%L
 * GWT Experiments
 * %%
 * Copyright (C) 2018 - 2022 INRAE AgroClim
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.KeyCodes;
import com.google.gwt.event.dom.client.KeyUpEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HasText;
import com.google.gwt.user.client.ui.Widget;

/**
 * Implementation of HelloView.
 */
public class Gwtbootstrap3ViewImpl extends Composite implements Gwtbootstrap3View {

    /**
     * UI binder.
     */
    interface Binder extends UiBinder<Widget, Gwtbootstrap3ViewImpl> {
    }

    /**
     * UI binder.
     */
    private static final Binder UI_BINDER = GWT.create(Binder.class);

    /**
     * Button to send message.
     */
    @UiField
    protected Button button;

    /**
     * Label to display errors.
     */
    @UiField
    protected HasText errorLabel;

    /**
     * Presenter of Hello.
     */
    private Presenter presenter;

    /**
     * Input box.
     */
    @UiField
    protected TextBox textBox;

    /**
     * Constructor.
     */
    public Gwtbootstrap3ViewImpl() {
        final GwtBootstrap3EntryPoint ep = new GwtBootstrap3EntryPoint();
        ep.onModuleLoad();
        initWidget(UI_BINDER.createAndBindUi(this));
        // Focus the cursor on the name field when the app loads
        textBox.setFocus(true);
        textBox.selectAll();
    }

    @Override
    public final void clearErrorLabel() {
        errorLabel.setText("");
    }

    @Override
    public final void disableSendButton() {
        button.setEnabled(false);
    }

    @Override
    public final void enableAndFocusSendButton() {
        button.setEnabled(true);
        button.setFocus(true);
    }

    /**
     * When user clicks on the Send button.
     *
     * @param e event not used
     */
    @UiHandler("button")
    public final void onClickSend(final ClickEvent e) {
        presenter.send(textBox.getText());
    }

    /**
     * When user types.
     *
     * @param event key event.
     */
    @UiHandler("textBox")
    public final void onKeyUp(final KeyUpEvent event) {
        if (event.getNativeKeyCode() == KeyCodes.KEY_ENTER) {
            presenter.send(textBox.getText());
        }
    }

    @Override
    public final void setErrorLabelText(final String text) {
        errorLabel.setText(text);
    }

    @Override
    public final void setName(final String name) {
        textBox.setText(name);
    }

    @Override
    public final void setPresenter(final Presenter p) {
        this.presenter = p;
    }
}
