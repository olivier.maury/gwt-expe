package fr.inrae.agroclim.gwtexpe.client.websocket;

/*-
 * #%L
 * GWT Experiments
 * %%
 * Copyright (C) 2018 - 2022 INRAE AgroClim
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.gwt.activity.shared.Activity;

import fr.inrae.agroclim.gwtexpe.client.ClientFactory;
import fr.inrae.agroclim.gwtexpe.client.HasActivityPlace;
import fr.inrae.agroclim.gwtexpe.client.HasNamePlace;
import fr.inrae.agroclim.gwtexpe.client.HasNameTokenizer;

/**
 * Place for WebSocket.
 */
public final class WebSocketPlace extends HasNamePlace implements HasActivityPlace {

    /**
     * The tokenizer for WebSocket.
     */
    public static class Tokenizer extends HasNameTokenizer<WebSocketPlace> {
        /**
         * Constructor.
         */
        public Tokenizer() {
            super(WebSocketPlace::new);
        }
    }

    /**
     * Constructor.
     *
     * @param token
     *            place token
     */
    public WebSocketPlace(final String token) {
        super(token);
    }

    @Override
    public Activity getActivity(final ClientFactory clientFactory) {
        return new WebSocketActivity(clientFactory);
    }

}
