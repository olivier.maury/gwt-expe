package fr.inrae.agroclim.gwtexpe.client.websocket;

/*-
 * #%L
 * GWT Experiments
 * %%
 * Copyright (C) 2018 - 2022 INRAE AgroClim
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import static jsinterop.annotations.JsPackage.GLOBAL;
import jsinterop.annotations.JsType;

/**
 * EventTarget is an interface implemented by objects that can receive events
 * and may have listeners for them.
 */
@JsType(isNative = true, namespace = GLOBAL)
public class EventTarget {
    /**
     * Register an event handler of a specific event type on the EventTarget.
     *
     * @param type
     *            A case-sensitive string representing the event type to listen
     *            for.
     * @param listener
     *            The object which receives a notification when an event of the
     *            specified type occurs.
     */
    public native void addEventListener(String type, Function listener);

}
