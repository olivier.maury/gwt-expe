package fr.inrae.agroclim.gwtexpe.client.ui;

/*-
 * #%L
 * GWT Experiments
 * %%
 * Copyright (C) 2018 - 2022 INRAE AgroClim
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.Scheduler.ScheduledCommand;
import com.google.gwt.dom.client.Document;
import com.google.gwt.dom.client.Element;
import com.google.gwt.dom.client.LIElement;
import com.google.gwt.dom.client.NodeList;
import com.google.gwt.dom.client.StyleInjector;
import com.google.gwt.dom.client.UListElement;
import com.google.gwt.place.shared.Place;
import com.google.gwt.place.shared.PlaceChangeEvent;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.ui.Anchor;
import com.google.gwt.user.client.ui.ComplexPanel;

import fr.inrae.agroclim.gwtexpe.client.AppPlaceHistoryMapper;
import fr.inrae.agroclim.gwtexpe.client.ui.MenuResource.Style;

public final class Menu extends ComplexPanel
implements PlaceChangeEvent.Handler {
    /**
     * The place history mapper of the app.
     */
    private final AppPlaceHistoryMapper historyMapper = GWT
            .create(AppPlaceHistoryMapper.class);

    /**
     * Style of menu.
     */
    private final Style style = MenuResource.INSTANCE.style();

    /**
     * Root element.
     */
    private final UListElement ul;

    public Menu() {
        final Element elem = Document.get().createElement("nav");
        elem.setAttribute("role", "navigation");
        elem.addClassName(style.topnav());
        setElement(elem);

        ul = Document.get().createULElement();
        elem.appendChild(ul);

        addItem("=", () -> getElement().toggleClassName(style.responsive()),
                style.icon());
    }

    public void addHome(final String title) {
        addItem(title, "#", style.home());
    }

    public void addHome(final String title, final Place place) {
        addItem(title, "#" + historyMapper.getToken(place), style.home());
    }

    private void addItem(final Anchor anchor, final String styleName) {
        final LIElement li = Document.get().createLIElement();
        li.addClassName(styleName);
        add(anchor, li);
        ul.appendChild(li);
    }

    public void addItem(final String title, final Place place) {
        addItem(title, "#" + historyMapper.getToken(place), style.item());
    }

    private void addItem(final String title, final Runnable cmd,
            final String styleName) {
        final Anchor anchor = new Anchor(title);
        anchor.addClickHandler(e -> cmd.run());
        addItem(anchor, styleName);
    }

    /**
     * Constructs a new menu item that fires a command when it is selected.
     *
     * @param title
     *            the item's title
     * @param cmd
     *            the command to be fired when it is clicked
     */
    public void addItem(final String title, final ScheduledCommand cmd) {
        final Anchor anchor = new Anchor(title);
        if (cmd != null) {
            anchor.addClickHandler(e -> cmd.execute());
        }
        anchor.addClickHandler(
                e -> getElement().removeClassName(style.responsive()));
        addItem(anchor, style.item());
    }

    private void addItem(final String title, final String href,
            final String styleName) {
        final Anchor anchor = new Anchor(title);
        anchor.getElement().setAttribute("href", href);
        anchor.addClickHandler(
                e -> getElement().removeClassName(style.responsive()));
        addItem(anchor, styleName);
    }

    private void addSubItem(final String parentTitle, final Anchor anchor, final String styleName) {
        final NodeList<Element> items = ul.getElementsByTagName("a");
        Element parentItem = null;
        for (int i = 0; i < items.getLength(); i++) {
            final Element item = items.getItem(i);
            if (parentTitle.equals(item.getInnerText())) {
                parentItem = item;
                break;
            }
        }
        if (parentItem == null) {
            throw new IllegalArgumentException("No menu item found for title=" + parentTitle);
        }
        final Element parentLi = parentItem.getParentElement();
        final NodeList<Element> uls = parentLi.getElementsByTagName("ul");
        Element ulElem = null;
        if (uls.getLength() == 0) {
            ulElem = Document.get().createULElement();
            parentLi.appendChild(ulElem);
        } else {
            ulElem = uls.getItem(0);
        }
        final LIElement li = Document.get().createLIElement();
        li.addClassName(styleName);
        add(anchor, li);
        ulElem.appendChild(li);
    }

    public void addSubItem(final String parentTitle, final String title, final Place place) {
        addSubItem(parentTitle, title, "#" + historyMapper.getToken(place), style.item());
    }

    public void addSubItem(final String parentTitle, final String title, final ScheduledCommand cmd) {
        final Anchor anchor = new Anchor(title);
        if (cmd != null) {
            anchor.addClickHandler(e -> cmd.execute());
        }
        anchor.addClickHandler(
                e -> getElement().removeClassName(style.responsive()));
        addSubItem(parentTitle, anchor, style.item());
    }

    private void addSubItem(final String parentTitle, final String title, final String href,
            final String styleName) {
        final Anchor anchor = new Anchor(title);
        anchor.getElement().setAttribute("href", href);
        anchor.addClickHandler(
                e -> getElement().removeClassName(style.responsive()));
        addSubItem(parentTitle, anchor, styleName);
    }

    @Override
    protected void onAttach() {
        super.onAttach();
        style.ensureInjected();
        StyleInjector.injectAtEnd(MenuResource.INSTANCE.mobile().getText());
    }

    @Override
    public void onPlaceChange(final PlaceChangeEvent event) {
        final String hash = "#" + historyMapper.getToken(event.getNewPlace());
        for (int i = 0; i < ul.getChildCount(); i++) {
            final Element child = DOM.getChild(ul, i);
            child.removeClassName(style.active());
        }
        for (int i = 0; i < ul.getChildCount(); i++) {
            final Element li = DOM.getChild(ul, i);
            final Element child = li.getFirstChildElement();
            GWT.log("i: " + i + " = " + child.getAttribute("href"));
            if (child.getAttribute("href").equals(hash)) {
                li.addClassName(style.active());
            }
        }
    }
}
