package fr.inrae.agroclim.gwtexpe.client.gwtbootstrap3;

/*-
 * #%L
 * GWT Experiments
 * %%
 * Copyright (C) 2018 - 2022 INRAE AgroClim
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.gwt.user.client.ui.IsWidget;

/**
 * View interface for GwtBoostrap3.
 *
 * In MVP development, a view is defined by an interface, which allows multiple
 * view implementations based on client characteristics (such as mobile vs.
 * desktop) and also facilitates lightweight unit testing by avoiding the
 * time-consuming GWTTestCase.
 */
public interface Gwtbootstrap3View extends IsWidget {
    /**
     * Presenter with needed methods from the view.
     */
    interface Presenter {
        /**
         * Sending message to the server.
         *
         * @param text text to send.
         */
        void send(String text);
    }

    /**
     * Remove errors in label.
     */
    void clearErrorLabel();

    /**
     * Disable the Send button.
     */
    void disableSendButton();

    /**
     * Enable and focus the Send button.
     */
    void enableAndFocusSendButton();

    /**
     * @param text error text to display
     */
    void setErrorLabelText(String text);

    /**
     * @param name user's name
     */
    void setName(String name);

    /**
     * @param presenter related presenter
     */
    void setPresenter(Presenter presenter);
}
