package fr.inrae.agroclim.gwtexpe.client.leaflet;

/*-
 * #%L
 * GWT Experiments
 * %%
 * Copyright (C) 2018 - 2022 INRAE AgroClim
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.Arrays;

import org.peimari.gleaflet.client.Map;
import org.peimari.gleaflet.client.TileLayer;
import org.peimari.gleaflet.client.TileLayerOptions;
import org.peimari.gleaflet.client.control.Layers;
import org.peimari.gleaflet.client.control.LayersOptions;
import org.peimari.gleaflet.client.googlemutant.GoogleMutant;
import org.peimari.gleaflet.client.googlemutant.GoogleMutantOptions;

/**
 * Helper class for Leaflet maps.
 */
public abstract class LeafletUtils {

    /**
     * @param map map where layers will be added
     * @return layers control.
     */
    public static Layers createLayersControl(final Map map) {
        final LayersOptions layersOptions = LayersOptions.create();
        final Layers layersControl = Layers.create(layersOptions);

        final TileLayerOptions osmOptions = TileLayerOptions.create();
        osmOptions.setSubDomains("a", "b", "c");
        osmOptions.setAttribution("(c) OpenStreetMap");
        final TileLayer osm = TileLayer.create("http://{s}.tile.osm.org/{z}/{x}/{y}.png", osmOptions);
        map.addLayer(osm);
        layersControl.addBaseLayer(osm, "OpenStreetMap");

        Arrays.asList("roadmap", "satellite", "terrain", "hybrid").forEach(type -> {
            final GoogleMutantOptions gmapsOptions = GoogleMutantOptions.create();
            gmapsOptions.setAttribution("(c) Google");
            gmapsOptions.setType(type);
            final GoogleMutant gmaps = GoogleMutant.create(gmapsOptions);
            map.addLayer(gmaps);
            layersControl.addBaseLayer(gmaps, "Google Map " + type);
        });
        return layersControl;
    }

}
