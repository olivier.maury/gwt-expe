package fr.inrae.agroclim.gwtexpe.client.leaflet;

/*-
 * #%L
 * GWT Experiments
 * %%
 * Copyright (C) 2018 - 2022 INRAE AgroClim
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.peimari.gleaflet.client.LayerGroup;
import org.peimari.gleaflet.client.Marker;
import org.vaadin.gleaflet.markercluster.client.AnimationEndListener;
import org.vaadin.gleaflet.markercluster.client.ClusterClickListener;
import org.vaadin.gleaflet.markercluster.client.MarkerClusterGroupOptions;
import org.vaadin.gleaflet.markercluster.client.resources.LeafletMarkerClusterResourceInjector;

import com.google.gwt.core.client.JavaScriptObject;

/**
 * Hack of original.
 */
public class MarkerClusterGroup extends LayerGroup {

    static {
        LeafletMarkerClusterResourceInjector.ensureInjected();
    }

    public static native MarkerClusterGroup create(MarkerClusterGroupOptions options)
    /*-{
        return new $wnd.L.MarkerClusterGroup(options);
    }-*/;

    protected MarkerClusterGroup() {
    }

    public final native void addAnimationEndListener(AnimationEndListener listener)
    /*-{
        var fn = $entry(function(e) {
            listener.@org.vaadin.gleaflet.markercluster.client.AnimationEndListener::onAnimationEnd()();
        });
        fn.prototype['gname'] = "animationend";
        this.on(fn.prototype['gname'], fn);
    }-*/;

    public final native JavaScriptObject addClusterClickListener(ClusterClickListener listener)
    /*-{
        var fn = $entry(function(e) {
            listener.@org.vaadin.gleaflet.markercluster.client.ClusterClickListener::onClick(
                Lorg/peimari/gleaflet/client/MouseEvent;
                Lorg/vaadin/gleaflet/markercluster/client/MarkerCluster;)(e, e.layer);
        });
        fn.prototype['gname'] = "clusterclick";
        this.on(fn.prototype['gname'], fn);
        return fn;
    }-*/;

    /**
     * Hacked method (not static).
     *
     * @param marker marker to add as a layer
     */
    public final native void addLayer(Marker marker)
    /*-{
        this.addLayer(marker)
    }-*/;

    public final native void removeAnimationEndListener()
    /*-{
        this.off("animationend");
    }-*/;
}
