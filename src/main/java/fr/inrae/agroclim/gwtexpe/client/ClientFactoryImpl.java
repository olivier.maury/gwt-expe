package fr.inrae.agroclim.gwtexpe.client;

/*-
 * #%L
 * GWT Experiments
 * %%
 * Copyright (C) 2018 - 2022 INRAE AgroClim
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.gwt.event.shared.SimpleEventBus;
import com.google.gwt.place.shared.PlaceController;
import com.google.web.bindery.event.shared.EventBus;

import fr.inrae.agroclim.gwtexpe.client.goodbye.GoodbyeView;
import fr.inrae.agroclim.gwtexpe.client.goodbye.GoodbyeViewImpl;
import fr.inrae.agroclim.gwtexpe.client.gwtbootstrap3.Gwtbootstrap3View;
import fr.inrae.agroclim.gwtexpe.client.gwtbootstrap3.Gwtbootstrap3ViewImpl;
import fr.inrae.agroclim.gwtexpe.client.hello.HelloView;
import fr.inrae.agroclim.gwtexpe.client.hello.HelloViewImpl;
import fr.inrae.agroclim.gwtexpe.client.leaflet.LeafletClusterView;
import fr.inrae.agroclim.gwtexpe.client.leaflet.LeafletClusterViewImpl;
import fr.inrae.agroclim.gwtexpe.client.leaflet.LeafletMapView;
import fr.inrae.agroclim.gwtexpe.client.leaflet.LeafletMapViewImpl;
import fr.inrae.agroclim.gwtexpe.client.ui.UiView;
import fr.inrae.agroclim.gwtexpe.client.ui.UiViewImpl;
import fr.inrae.agroclim.gwtexpe.client.upload.UploadView;
import fr.inrae.agroclim.gwtexpe.client.upload.UploadViewImpl;
import fr.inrae.agroclim.gwtexpe.client.websocket.WebSocketView;
import fr.inrae.agroclim.gwtexpe.client.websocket.WebSocketViewImpl;

/**
 * The only one implementation of ClientFactory.
 */
public final class ClientFactoryImpl implements ClientFactory {

    /**
     * Dispatcher of {@link Event}s to interested parties.
     */
    private final EventBus eventBus = new SimpleEventBus();

    /**
     * View for Goodbye.
     */
    private final GoodbyeView goodbyeView = new GoodbyeViewImpl();

    /**
     * View for Goodbye.
     */
    private final Gwtbootstrap3View gwtbootstrap3View = new Gwtbootstrap3ViewImpl();

    /**
     * View for Hello.
     */
    private final HelloView helloView = new HelloViewImpl();

    /**
     * View for Leaflet test page.
     */
    private final LeafletClusterView leafletView = new LeafletClusterViewImpl();

    /**
     * View for Leaflet test page.
     */
    private final LeafletMapView leafletMapView = new LeafletMapViewImpl();

    /**
     * View for upload form.
     */
    private final UploadView uploadView = new UploadViewImpl();

    /**
     * Main page composite.
     */
    private final MainPage mainPage = new MainPageImpl(this);

    /**
     * Controller in charge of the user's location in the app.
     */
    private final PlaceController placeController = new PlaceController(
            eventBus);

    /**
     * View for WebSocket test page.
     */
    private final WebSocketView webSocketView = new WebSocketViewImpl();

    /**
     * View for UI widget demo.
     */
    private final UiView uiView = new UiViewImpl();

    @Override
    public EventBus getEventBus() {
        return eventBus;
    }

    @Override
    public GoodbyeView getGoodbyeView() {
        return goodbyeView;
    }

    @Override
    public Gwtbootstrap3View getGwtbootstrap3View() {
        return gwtbootstrap3View;
    }

    @Override
    public HelloView getHelloView() {
        return helloView;
    }

    @Override
    public LeafletMapView getLeafletMapView() {
        return leafletMapView;
    }

    @Override
    public LeafletClusterView getLeafletView() {
        return leafletView;
    }

    @Override
    public MainPage getMainPage() {
        return mainPage;
    }

    @Override
    public PlaceController getPlaceController() {
        return placeController;
    }

    @Override
    public UiView getUiView() {
        return uiView;
    }

    @Override
    public UploadView getUploadView() {
        return uploadView;
    }

    @Override
    public WebSocketView getWebSocketView() {
        return webSocketView;
    }

}
