package fr.inrae.agroclim.gwtexpe.client.goodbye;

/*-
 * #%L
 * GWT Experiments
 * %%
 * Copyright (C) 2018 - 2022 INRAE AgroClim
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.gwt.dom.client.Element;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.SimplePanel;

/**
 * Implementation of GoodbyeView.
 */
public class GoodbyeViewImpl extends Composite implements GoodbyeView {

    /**
     * Element to display text.
     */
    private final Element nameSpan = DOM.createDiv();

    /**
     * Root element for the view.
     */
    private final SimplePanel viewPanel = new SimplePanel();

    /**
     * Constructor.
     */
    public GoodbyeViewImpl() {
        viewPanel.getElement().appendChild(nameSpan);
        initWidget(viewPanel);
    }

    @Override
    public final void setName(final String name) {
        nameSpan.setInnerText("Good-bye, " + name);
    }

    @Override
    public void setPresenter(final GoodbyeView.Presenter presenter) {
    }
}
