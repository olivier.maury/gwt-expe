package fr.inrae.agroclim.gwtexpe.server;

/*-
 * #%L
 * GWT Experiments
 * %%
 * Copyright (C) 2018 - 2022 INRAE AgroClim
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.ResourceBundle;

import fr.inrae.agroclim.gwtexpe.server.model.ApplicationInfo;
import lombok.extern.log4j.Log4j2;

/**
 * Helper for application.
 */
@Log4j2
public abstract class ApplicationUtils {
    /**
     * @return application info.
     */
    public static ApplicationInfo getInfo() {
        final ResourceBundle res = ResourceBundle.getBundle("fr.inrae.agroclim.gwtexpe.version");
        final SimpleDateFormat parser = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        Date buildDate;
        try {
            buildDate = parser.parse(res.getString("build.date"));
        } catch (final ParseException e) {
            LOGGER.error("Parsing failed for: " + res.getString("build.date"), e);
            buildDate = null;
        }
        final ApplicationInfo info = new ApplicationInfo();
        info.setName("JavaEE GWT Example");
        info.setBuildDate(buildDate);
        info.setVersion(res.getString("version"));
        return info;
    }
}
