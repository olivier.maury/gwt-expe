package fr.inrae.agroclim.gwtexpe.server.model;

/*-
 * #%L
 * GWT Experiments
 * %%
 * Copyright (C) 2018 - 2022 INRAE AgroClim
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.io.Serializable;
import java.util.Date;

import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonFormat;

import fr.inrae.agroclim.gwtexpe.shared.HasName;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * App info.
 */
@ToString
@XmlRootElement(name = "ApplicationInfo", namespace = "http://gwtexpe.agroclim.inrae.fr")
public class ApplicationInfo implements HasName, Serializable {

    /**
     * UUID for Serializable.
     */
    private static final long serialVersionUID = 7222130727811174436L;

    /**
     * Date of application build.
     */
    @Getter
    @Setter
    @JsonFormat(
            shape = JsonFormat.Shape.STRING,
            pattern = "yyyy-MM-dd HH:mm:ss",
            timezone = "CET"
            )
    @Schema(description = "Date when application was built.")
    private Date buildDate;

    /**
     * Application name.
     */
    @Getter
    @Setter
    private String name;

    /**
     * Application version.
     */
    @Getter
    @Setter
    private String version;

    /**
     * No-arg constructor for JAX-RS.
     */
    public ApplicationInfo() {
    }
}
