/**
 * JPA entity for table "person".
 */
package fr.inrae.agroclim.gwtexpe.server.model;

/*-
 * #%L
 * GWT Experiments
 * %%
 * Copyright (C) 2018 - 2022 INRAE AgroClim
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import fr.inrae.agroclim.gwtexpe.shared.HasName;
import lombok.Data;
import lombok.ToString;

/**
 * JPA Entity Person.
 */
@Data
@Entity
@Table(name = "person")
@ToString
public class Person implements HasName, Serializable {
    /**
     * UUID for Serializable.
     */
    private static final long serialVersionUID = 3228252165929549109L;

    /**
     * Creation date.
     */
    @Column(name = "created", nullable = false)
    private LocalDateTime created = LocalDateTime.now();

    /**
     * ID.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private long id;

    /**
     * Person's name.
     */
    @Column(name = "name")
    private String name;
}
