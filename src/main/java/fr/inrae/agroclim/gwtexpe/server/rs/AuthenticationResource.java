package fr.inrae.agroclim.gwtexpe.server.rs;

/*-
 * #%L
 * GWT Experiments
 * %%
 * Copyright (C) 2018 - 2022 INRAE AgroClim
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.io.UnsupportedEncodingException;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import fr.inrae.agroclim.gwtexpe.server.dao.PersonDao;
import fr.inrae.agroclim.gwtexpe.server.jwt.JsonWebTokenService;
import fr.inrae.agroclim.gwtexpe.server.model.Person;
import fr.inrae.agroclim.gwtexpe.server.model.User;
import fr.inrae.agroclim.gwtexpe.server.rs.filter.AuthenticatedUser;
import fr.inrae.agroclim.gwtexpe.server.rs.filter.Secured;
import lombok.extern.log4j.Log4j2;

/**
 * Authentication JAX-RS resource.
 */
@Log4j2
@Path("auth")
@RequestScoped
public class AuthenticationResource {
    /**
     * CDI injected from AuthenticatedUserProducer, fired by
     * AuthenticationFilter.
     */
    @Inject
    @AuthenticatedUser
    private User authenticatedUser;

    /**
     * DAO for User.
     */
    @Inject
    private PersonDao personDao;

    /**
     * Authenticate against a database, LDAP, file or whatever.
     *
     * @param username username
     * @param password password
     * @return response
     */
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Path("token")
    public Response authenticateUser(
            final @FormParam("username") String username,
            final @FormParam("password") String password) {

        // Authenticate the user using the credentials provided
        if (!authenticate(username, password)) {
            return Response.status(Response.Status.FORBIDDEN).build();
        } else {

            // Issue a token for the user
            String token = issueToken(username);

            // Return the token on the response
            return Response.ok(token).build();

        }
    }

    /**
     * This method is annotated with @Secured. The authentication filter will be
     * executed before invoking this method. The HTTP request must be performed
     * with a valid token.
     *
     * @return user's name or "unknown"
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("info")
    @Secured
    public String info() {
        if (authenticatedUser != null) {
            return authenticatedUser.toString();
        }
        return "unknown";
    }

    /**
     * Authenticate against a database, LDAP, file or whatever.
     *
     * @param username username
     * @param password password
     * @return true if the credentials are valid
     */
    private boolean authenticate(final String username, final String password) {
        Person person = personDao.findBy(username);
        return person != null && person.getName().equals(password);
    }

    /**
     * Issue a JWT token, associated to a user.
     *
     * @param username user's name
     * @return the issued token
     */
    private String issueToken(final String username) {
        Person person = personDao.findBy(username);
        if (person == null) {
            LOGGER.warn("Strange '{}' is unknown!", username);
            return  null;
        }
        User user = new User();
        user.setCreated(person.getCreated());
        user.setId(person.getId());
        user.setName(person.getName());
        JsonWebTokenService jwt = new JsonWebTokenService();
        try {
            return jwt.generateToken(user);
        } catch (IllegalArgumentException | UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
    }
}
