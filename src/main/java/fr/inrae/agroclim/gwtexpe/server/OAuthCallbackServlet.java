package fr.inrae.agroclim.gwtexpe.server;

/*-
 * #%L
 * GWT Experiments
 * %%
 * Copyright (C) 2018 - 2022 INRAE AgroClim
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.io.IOException;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.google.gwt.thirdparty.guava.common.base.Objects;

import fr.inrae.agroclim.gwtexpe.server.Configuration.Key;
import fr.inrae.agroclim.gwtexpe.server.websocket.SessionHandler;
import lombok.extern.log4j.Log4j2;

/**
 * The callback URL for OAuth2, after user authenticates.
 */
@RequestScoped
@WebServlet(OAuthCallbackServlet.RESOURCE)
@Log4j2
public class OAuthCallbackServlet extends HttpServlet {
    /**
     * serial ID.
     */
    private static final long serialVersionUID = 1228252165929549102L;

    /**
     * Resource URL for the callback.
     */
    public static final String RESOURCE = "/oauth2/authorized";

    /**
     * WebSocket session handler.
     */
    @Inject
    private SessionHandler sessionHandler;

    /**
     * Application configuration from context.xml.
     */
    @Inject
    private Configuration config;

    /**
     * User representation.
     */
    @Inject
    private You you;

    @Override
    public final void doGet(final HttpServletRequest request,
            final HttpServletResponse response) throws IOException {
        LOGGER.traceEntry();
        /*
         * code - The server returns the authorization code in the query string
         * state - The server returns the same state value that you passed
         *
         * You should first compare this state value to ensure it matches the
         * one you started with. You can typically store the state value in a
         * cookie or session, and compare it when the user comes back. This
         * ensures your redirection endpoint isn't able to be tricked into
         * attempting to exchange arbitrary authorization codes.
         */
        final String code = request.getParameter("code");
        final String state = request.getParameter("state");
        final String error = request.getParameter("error");
        final String errorDescription = request
                .getParameter("error_description");
        LOGGER.trace("code={}", code);
        LOGGER.trace("state={}", state);
        LOGGER.trace("error={}", error);
        LOGGER.trace("errorDescription={}", errorDescription);

        final HttpSession session = request.getSession(true);
        final String randomString = (String) session
                .getAttribute("oauth-state");
        LOGGER.trace("randomString={}", state);
        if (!Objects.equal(randomString, state)) {
            response.getWriter().write("state does not match!");
            response.sendError(HttpServletResponse.SC_FORBIDDEN);
            return;
        }

        if (error != null) {
            LOGGER.error(error);
            LOGGER.error(errorDescription);
            return;
        }
        final OAuthB2Access b2access = new OAuthB2Access();
        b2access.setAppUrl(config.get(Key.APP_URL));
        b2access.setServer(OAuthB2Access.Server.valueOf(config.get(Key.B2ACCESS_SERVER)));
        b2access.setClientId(config.get(Key.B2ACCESS_CLIENT_ID));
        b2access.setClientId(config.get(Key.B2ACCESS_CLIENT_SECRET));
        // /oauth2/token
        if (b2access.retrieveToken(code)) {
            // /oauth2/userinfo
            final OAuthB2Access.UserInfo info = b2access.getUserInfo();
            LOGGER.trace("user = {}", info);
            if (sessionHandler != null) {
                sessionHandler.sendToAllConnectedSessions(info.toString());
            }
            you.setName(info.getUsername());
            response.sendRedirect(b2access.getAppUrl());
        }
    }
}
