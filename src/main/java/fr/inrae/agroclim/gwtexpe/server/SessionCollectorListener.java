package fr.inrae.agroclim.gwtexpe.server;

/*-
 * #%L
 * GWT Experiments
 * %%
 * Copyright (C) 2018 - 2022 INRAE AgroClim
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

import lombok.Getter;
import lombok.extern.log4j.Log4j2;

/**
 * Keep all active user sessions in application ServletContext attribute.
 *
 * Last changed : $Date: 2016-03-08 12:13:07 +0100 (mar. 08 mars 2016) $
 *
 * @author $Author: omaury $
 * @version $Revision: 947 $
 */
@Log4j2
@WebListener
public final class SessionCollectorListener
implements ServletContextListener, HttpSessionListener {

    /**
     * Attribute in HttpSession to store account ID.
     */
    @Getter
    private static String accountIdAttribute = "accountId";

    /**
     * Attribute of application session to store user sessions list.
     */
    @Getter
    private static String applicationSessionListAttribute =
    SessionCollectorListener.class.getCanonicalName();

    /**
     * @param context servlet context
     * @return list of sessions
     */
    public static List<HttpSession> getSessions(final ServletContext context) {
        final List<HttpSession> sessions = new ArrayList<>();
        final Object objs;
        objs = context.getAttribute(applicationSessionListAttribute);
        if (!(objs instanceof ArrayList)) {
            LOGGER.warn("context attribute {} is not an ArrayList!",
                    applicationSessionListAttribute);
            return sessions;
        }
        final List<?> list = (List<?>) objs;
        list.forEach((obj) -> {
            if (obj instanceof HttpSession) {
                sessions.add((HttpSession) obj);
            }
        });
        LOGGER.trace("Number of sessions = " + sessions.size());
        return sessions;
    }

    /**
     * Iterate over sessions in getSessions() and invalidate the sessions
     * matching accountId.
     *
     * @param context
     *            servlet context
     * @param accountId
     *            account of sessions to invalidate.
     */
    public static void invalidateSessions(final ServletContext context,
            final Integer accountId) {
        HttpSession toInvalid = null;
        for (final HttpSession session : getSessions(context)) {
            if (Objects.equals(session.getAttribute(accountIdAttribute),
                    accountId)) {
                toInvalid = session;
            }
        }
        if (toInvalid != null) {
            toInvalid.invalidate();
        }
    }

    /**
     * ServletContext.
     */
    private ServletContext context;

    @Override
    public void contextDestroyed(final ServletContextEvent sce) {
    }

    @Override
    public void contextInitialized(final ServletContextEvent sce) {
        LOGGER.traceEntry();
        context = sce.getServletContext();
        context.setAttribute(applicationSessionListAttribute,
                new ArrayList<>());
    }

    @Override
    public void sessionCreated(final HttpSessionEvent event) {
        LOGGER.traceEntry();
        getSessions(context).add(event.getSession());
        LOGGER.debug(
                "Session created; session id = " + event.getSession().getId());
        LOGGER.debug("Number of sessions = " + getSessions(context).size());
    }

    @Override
    public void sessionDestroyed(final HttpSessionEvent event) {
        getSessions(context).remove(event.getSession());

        LOGGER.debug("Session destroyed; session id = "
                + event.getSession().getId());
        LOGGER.debug("Number of sessions = " + getSessions(context).size());
    }
}
