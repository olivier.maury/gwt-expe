package fr.inrae.agroclim.gwtexpe.server;

/*-
 * #%L
 * GWT Experiments
 * %%
 * Copyright (C) 2018 - 2022 INRAE AgroClim
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Observes;
import javax.inject.Named;

import lombok.extern.log4j.Log4j2;

/**
 * Asynchronous processing of user entries.
 *
 * If it is not asynchronous, when the user types the name, the response is delayed.
 */
@ApplicationScoped
@Log4j2
public class UsernameProcessor {

    /**
     * Duration of fake processing.
     */
    private static final long DELAY = 5_000;

    /**
     * All typed usernames.
     */
    private final Queue<String> usernames = new ConcurrentLinkedQueue<>();

    /**
     * The processor is processing an entry.
     */
    private boolean processing = false;

    /**
     * The loop for the processing.
     */
    private final Runnable processor = () -> {
        processing = true;
        LOGGER.trace("Size of queue: {}", usernames.size());
        for (final String username : usernames) {
            LOGGER.trace("Processing {}", username);
            // it takes a long time !
            try {
                Thread.sleep(DELAY);
            } catch (final InterruptedException e) {
                // ..
            }
            usernames.remove(username);
            LOGGER.trace("{} is processed.", username);
        }
        processing = false;
    };

    /**
     * Add the typed named to the queue for the asynchronous processing.
     *
     * @param username typed name.
     */
    public void process(final @Observes @Named("username") String username) {
        LOGGER.traceEntry(username);
        usernames.add(username);
        if (processing) {
            LOGGER.trace("Currently processing. Wait to process.");
            return;
        }
        new Thread(processor).start();
        LOGGER.traceExit();
    }
}
