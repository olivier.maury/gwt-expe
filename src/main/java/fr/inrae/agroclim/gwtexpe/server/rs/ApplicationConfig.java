package fr.inrae.agroclim.gwtexpe.server.rs;

/*-
 * #%L
 * GWT Experiments
 * %%
 * Copyright (C) 2018 - 2022 INRAE AgroClim
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.ws.rs.core.Application;

import fr.inrae.agroclim.gwtexpe.server.model.ApplicationInfo;
import fr.inrae.agroclim.gwtexpe.server.rs.filter.AuthenticationFilter;
import fr.inrae.agroclim.gwtexpe.server.rs.filter.LogFilter;
import io.swagger.v3.jaxrs2.integration.resources.AcceptHeaderOpenApiResource;
import io.swagger.v3.jaxrs2.integration.resources.OpenApiResource;
import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Contact;
import io.swagger.v3.oas.annotations.info.Info;
import io.swagger.v3.oas.annotations.info.License;

/**
 * JAX-RS application at "/rs".
 */
@OpenAPIDefinition(
        info = @Info(
                title = "the title",
                version = "0.0",
                description = "My API",
                license = @License(
                        name = "Apache 2.0",
                        url = "http://foo.bar"
                        ),
                contact = @Contact(
                        url = "http://gigantic-server.com",
                        name = "Fred",
                        email = "Fred@gigagantic-server.com"
                        )
                )
        )
@javax.ws.rs.ApplicationPath("rs")
public class ApplicationConfig extends Application {

    @Override
    public final Set<Class<?>> getClasses() {
        return Stream.of(
                // OpenAPI / Swagger
                OpenApiResource.class, AcceptHeaderOpenApiResource.class,
                // JAX-RS resources
                ApplicationResource.class, AuthenticationResource.class,
                SseResource.class, ZakeeService.class,
                // POJO
                ApplicationInfo.class,
                // Dependencies of resources
                AuthenticationFilter.class, LogFilter.class
                // -
                ).collect(Collectors.toSet());
    }
}
