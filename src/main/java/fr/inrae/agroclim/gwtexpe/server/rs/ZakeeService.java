package fr.inrae.agroclim.gwtexpe.server.rs;

/*-
 * #%L
 * GWT Experiments
 * %%
 * Copyright (C) 2018 - 2022 INRAE AgroClim
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import fr.inrae.agroclim.gwtexpe.server.Who;
import io.swagger.v3.oas.annotations.Operation;

/**
 * Should not be final.
 *
 * Apparently @Context only works when we explictly mark the scope as well.
 * Anyhow as per IBM it is a good practice to explicitly mark a RS service as
 * RequestScoped.
 *
 * OpenAPI annotations :
 * https://github.com/swagger-api/swagger-core/wiki/Swagger-2.X---Annotations
 */
@Path("zakee-service")
@RequestScoped
public class ZakeeService {

    /**
     * Portion of the request URI that indicates the context of the request.
     *
     * @see javax.servlet.HttpServletRequest#getContextPath()
     */
    private String contextPath;

    /**
     * Information for HTTP servlet.
     */
    @Context
    private HttpServletRequest httpServletContext;

    /**
     * Injection of @RequestScoped object.
     */
    @Inject
    private Who who;

    /**
     * Store once contextPath.
     */
    @PostConstruct
    public void init() {
        contextPath = httpServletContext.getContextPath();
    }

    /**
     * @return Context path of the request
     */
    @GET
    @Produces(MediaType.TEXT_HTML)
    @Path("context")
    @Operation(summary = "Get context path",
    description = "Get the context path of the request.")
    public String processContextPath() {
        return "<p>This " + contextPath + " service is life!</p>";
    }

    /**
     * Test injection of @RequestScoped object.
     *
     * @return string
     */
    @GET
    @Produces(MediaType.TEXT_HTML)
    @Path("who")
    @Operation(summary = "Test injection.",
    description = "Test injection of @RequestScoped object.")
    public String processWho() {
        return "<p>Service for " + who.getYou().getName() + "</p>";
    }
}
