package fr.inrae.agroclim.gwtexpe.server.dao;

/*-
 * #%L
 * GWT Experiments
 * %%
 * Copyright (C) 2018 - 2022 INRAE AgroClim
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.List;

import fr.inrae.agroclim.gwtexpe.server.model.Person;

/**
 * DAO for Person.
 */
public interface PersonDao {
    /**
     * @return all persons.
     */
    List<Person> findAll();

    /**
     * @param name
     *            person's name
     * @return found Person
     */
    Person findBy(String name);

    /**
     * @param id of person
     * @return person registered after id.
     */
    Person findNewerThan(long id);

    /**
     * @param name
     *            person's name
     * @return found or created Person
     */
    Person findOrCreateBy(String name);
}
