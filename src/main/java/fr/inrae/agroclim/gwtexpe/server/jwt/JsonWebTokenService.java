package fr.inrae.agroclim.gwtexpe.server.jwt;

/*-
 * #%L
 * GWT Experiments
 * %%
 * Copyright (C) 2018 - 2022 INRAE AgroClim
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.io.UnsupportedEncodingException;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTDecodeException;
import com.auth0.jwt.exceptions.SignatureVerificationException;
import com.auth0.jwt.exceptions.TokenExpiredException;
import com.auth0.jwt.interfaces.DecodedJWT;

import fr.inrae.agroclim.gwtexpe.server.model.User;
import lombok.NonNull;
import lombok.extern.log4j.Log4j2;

/**
 * Service that creates a Json Web Token that will be sent to the client. So
 * that the client can authenticate to call the different urls.
 */
@Named
@ApplicationScoped
@Log4j2
public final class JsonWebTokenService {
    /**
     * Specific Issuer ("iss") claim to the Payload.
     */
    private static final String AUTH0_JWT_ISSUER = "auth0";
    /**
     * Custom claim for user name.
     */
    private static final String JWT_ATTRIBUTE_NAME = "name";

    /**
     * Generate a Json Web Token.
     *
     * @param user
     *            user for the token
     * @return JWT string
     * @throws IllegalArgumentException
     *             raised by Algorithm
     * @throws UnsupportedEncodingException
     *             raised by Algorithm
     */
    public String generateToken(@NonNull final User user)
            throws IllegalArgumentException, UnsupportedEncodingException {
        if (user.getName() == null) {
            throw new IllegalArgumentException("User name must not be null!");
        }
        // 1 day
        final int expirationDelay = 1;

        final Calendar issuedAt = new GregorianCalendar();
        issuedAt.setTime(new Date());

        final Calendar expiresAt = new GregorianCalendar();
        expiresAt.setTime(new Date());
        expiresAt.add(Calendar.DAY_OF_YEAR, expirationDelay);

        LOGGER.debug("JWT exprired at: {}", expiresAt.getTime());

        return JWT.create().withIssuer(AUTH0_JWT_ISSUER)
                .withClaim(JWT_ATTRIBUTE_NAME, user.getName())
                .withIssuedAt(issuedAt.getTime())
                .withExpiresAt(expiresAt.getTime())
                .withNotBefore(issuedAt.getTime()).sign(getAlgorithm());
    }

    /**
     * @return Algorithm to sign
     * @throws IllegalArgumentException
     *             raised by HMAC256
     * @throws UnsupportedEncodingException
     *             raised by HMAC256
     */
    private Algorithm getAlgorithm()
            throws IllegalArgumentException, UnsupportedEncodingException {
        final String jwtSecret;
        jwtSecret = "eyJ0eXddcdcscefbgjnh,ghAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9";
        return Algorithm.HMAC256(jwtSecret);
    }

    /**
     * Validate token and extract the user data.
     *
     * @param token
     *            the JSON Web Token
     * @return the user data relative to the token.
     * @throws IllegalArgumentException
     *             raised by Algorithm
     * @throws UnsupportedEncodingException
     *             raised by Algorithm
     */
    public User retrieveUser(final String token)
            throws IllegalArgumentException, UnsupportedEncodingException {

        if (token == null) {
            return null;
        }
        // Reusable verifier instance
        final JWTVerifier verifier = JWT.require(getAlgorithm())
                .withIssuer(AUTH0_JWT_ISSUER)
                // 1 sec for nbf and iat and exp
                .acceptLeeway(1).build();
        final DecodedJWT jwt;
        try {
            jwt = verifier.verify(token);
        } catch (final JWTDecodeException e) {
            LOGGER.warn("JWTDecodeException! ", e);
            return null;
        } catch (final SignatureVerificationException e) {
            LOGGER.warn("SignatureVerificationException! ", e);
            return null;
        } catch (final TokenExpiredException e) {
            LOGGER.warn("TokenExpiredException! ", e);
            return null;
        }

        final User user = new User();
        user.setName(jwt.getClaim(JWT_ATTRIBUTE_NAME).asString());
        return user;
    }
}
