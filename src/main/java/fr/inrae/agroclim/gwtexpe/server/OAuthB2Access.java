package fr.inrae.agroclim.gwtexpe.server;

/*-
 * #%L
 * GWT Experiments
 * %%
 * Copyright (C) 2018 - 2022 INRAE AgroClim
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.SecureRandom;
import java.util.Arrays;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.StringJoiner;

import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.extern.log4j.Log4j2;

/**
 * OAuth2 handling for B2Access.
 *
 * See https://eudat.eu/services/userdoc/b2access-service-integration and
 * https://github.com/hummingbird-dev/OAuth_unity.eudat
 */
@Log4j2
public class OAuthB2Access {
    /**
     * Type of server.
     */
    @RequiredArgsConstructor
    enum Server {
        /**
         * Development server, INRA IDP not available, use ORCID.
         */
        DEVELOPMENT("https://unity.eudat-aai.fz-juelich.de"),
        /**
         * Integration server, registration is not automatic.
         */
        INTEGRATION("https://b2access-integration.fz-juelich.de"),
        /**
         * Production, with real URL.
         */
        PRODUCTION("https://b2access.eudat.eu");

        /**
         * Server URL.
         */
        @Getter
        private final String url;
    }

    /**
     * User info from REST resource.
     */
    @ToString
    public static class UserInfo {
        /**
         * email.
         */
        @Getter
        @Setter
        private String email;
        /**
         * email Verified.
         */
        @Getter
        @Setter
        private Boolean emailVerified;
        /**
         * uid.
         */
        @Getter
        @Setter
        private String uid;
        /**
         * user name.
         */
        @Getter
        @Setter
        private String username;
    }

    /**
     * Characters for randomString().
     */
    private static final String AB = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ"
            + "abcdefghijklmnopqrstuvwxyz";
    /**
     * 2nd URL called: get token.
     */
    private static final String ACCESS_TOKEN_URL = "/oauth2/token";
    /**
     * 1st URL called: authenticate the user.
     *
     * Web user will be redirected to /oauth2-as/oauth2-authz-web-entry.
     */
    private static final String AUTHORIZATION_URL = "/oauth2-as/oauth2-authz";
    /**
     * Random generator.
     */
    private static final SecureRandom RND = new SecureRandom();

    /**
     * 3rd URL called: get user info using token.
     */
    private static final String USERINFO_URL = "/oauth2/userinfo";

    /**
     * @param text
     *            text to encode
     * @return encoded text in Base64
     */
    private static String encodeBase64(final String text) {
        return new String(Base64.getEncoder().encode(text.getBytes()));
    }

    /**
     * Extract value from a JSON tree.
     *
     * @param json
     *            JSON string from the HTTP request
     * @param paths
     *            path in the JSON tree
     * @return value for the path
     */
    private static String extract(final String json, final List<String> paths) {
        final ObjectMapper objectMapper = new ObjectMapper();
        JsonNode node;
        try {
            node = objectMapper.readTree(json);
        } catch (final IOException e) {
            LOGGER.catching(e);
            return null;
        }
        boolean found = true;
        for (final String path : paths) {
            if (node.has(path)) {
                node = node.get(path);
            } else {
                found = false;
            }
        }
        if (found) {
            return node.asText();
        }
        return null;
    }

    /**
     * Extract value from a JSON tree.
     *
     * @param json
     *            JSON string from the HTTP request
     * @param property
     *            property in the JSON tree
     * @return value for the path
     */
    private static String extract(final String json, final String property) {
        final ObjectMapper objectMapper = new ObjectMapper();
        JsonNode node;
        try {
            node = objectMapper.readTree(json);
        } catch (final IOException e) {
            LOGGER.catching(e);
            return null;
        }
        if (node.has(property)) {
            return node.get(property).asText();
        }
        return null;
    }

    /**
     * For testing purpose.
     *
     * @param json
     *            JSON string from the HTTP request
     * @param paths
     *            path in the JSON tree
     * @return value for the path
     */
    public static String extract(final String json, final String[] paths) {
        return extract(json, Arrays.asList(paths));
    }

    /**
     * @param stream
     *            input or error stream from connection
     * @return full text from stream
     * @throws IOException
     *             exception while reading stream
     */
    private static String getResponseContent(final InputStream stream)
            throws IOException {
        final BufferedReader br = new BufferedReader(
                new InputStreamReader(stream));

        final StringJoiner sj = new StringJoiner("\n");
        String output;
        while ((output = br.readLine()) != null) {
            sj.add(output);
        }

        br.close();
        return sj.toString();
    }

    /**
     * Token to call resources.
     */
    private String accessToken = null;

    /**
     * URL of client application.
     */
    @Getter
    @Setter
    private String appUrl = "http://localhost:8080/gwtexpe";

    /**
     * ID of client application.
     */
    @Setter
    private String clientId = null;

    /**
     * Password of client application.
     */
    @Setter
    private String clientSecret = null;

    /**
     * State of user.
     */
    @Getter
    private String randomString = null;

    /**
     * Server mode.
     */
    @Setter
    private Server server;

    /**
     * @param resource
     *            resource with arguments in Jokolia API
     * @return JSON response
     */
    private String get(final String resource) {
        try {
            LOGGER.trace(server.getUrl() + resource);
            final URL url = new URL(server.getUrl() + resource);
            final HttpURLConnection conn = (HttpURLConnection) url
                    .openConnection();
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Accept", "application/json");
            if (accessToken != null) {
                conn.setRequestProperty("Authorization",
                        "Bearer " + accessToken);
            }

            if (conn.getResponseCode() != HttpServletResponse.SC_OK) {
                final String responseContent = getResponseContent(
                        conn.getErrorStream());
                conn.disconnect();
                LOGGER.error(responseContent);
                throw new RuntimeException(
                        "Failed : HTTP error code=" + conn.getResponseCode()
                        + ", response=" + responseContent);
            }

            final String responseContent = getResponseContent(
                    conn.getInputStream());
            conn.disconnect();
            return responseContent;

        } catch (final IOException e) {
            LOGGER.catching(e);
        }

        return null;
    }

    /**
     * @return URL of callback on the client application.
     */
    public final String getAuthorizeUrl() {
        Objects.requireNonNull(randomString, "randomString must not be null!");
        Objects.requireNonNull(clientId, "clientId must not be null!");
        final String scope = "profile,email";
        final String url = server.getUrl() + AUTHORIZATION_URL
                + "?response_type=code&client_id=" + clientId + "&redirect_uri="
                + getRedirectUrl() + "&scope=" + scope + "&state="
                + randomString;
        return url;
    }

    /**
     * @return basic authentication encoding for the HTTP header.
     */
    private String getBasicAuthentication() {
        Objects.requireNonNull(clientId, "clientId must not be null!");
        Objects.requireNonNull(clientSecret, "clientSecret must not be null!");
        final String userPassword = clientId + ":" + clientSecret;
        return encodeBase64(userPassword);
    }

    /**
     * @return URL on OAuth server to redirect the user.
     */
    public final String getRedirectUrl() {
        // Another URL can be used:
        // https://unity.eudat-aai.fz-juelich.de/oauth2-as/oauth2-authz
        // ?client_id=omaury-gwtexpe-bis
        // &redirect_uri=http://localhost:8080/gwtexpe/oauth2/authorized
        // &scope=profile,email&response_type=code
        // &state=14N8PON3olyCEV3G75xXTdrY28MsX8UVPFczM4XB
        return appUrl + OAuthCallbackServlet.RESOURCE;
    }

    /**
     * @return Info from B2Access REST resource
     */
    public final UserInfo getUserInfo() {
        if (accessToken == null) {
            throw new IllegalStateException("Access token not set!");
        }
        final String json = get(USERINFO_URL);
        LOGGER.trace(json);
        final String error = extract(json, "error");
        final String errorDescription = extract(json, "error_description");
        LOGGER.trace("error={}, error_description={}", error, errorDescription);
        final String uid = extract(json, "sub");
        final String username = extract(json, "name");
        final String email = extract(json, "email");
        final String emailVerified = extract(json, "email_verified");
        final UserInfo info = new UserInfo();
        info.setEmail(email);
        if (emailVerified == null) {
            info.setEmailVerified(null);
        }
        info.setUid(uid);
        info.setUsername(username);
        return info;
    }

    /**
     * @param len
     *            number of characters in the string
     * @return random string
     */
    public final String makeRandomString(final int len) {
        final StringBuilder sb = new StringBuilder(len);
        for (int i = 0; i < len; i++) {
            sb.append(AB.charAt(RND.nextInt(AB.length())));
        }
        randomString = sb.toString();
        return sb.toString();
    }

    /**
     * @param resource
     *            resource with arguments in Jokolia API
     * @param args
     *            data arguments
     * @return JSON response
     */
    private String post(final String resource, final Map<String, String> args) {
        try {
            LOGGER.trace(server.getUrl() + resource);
            final URL url = new URL(server.getUrl() + resource);

            final StringJoiner paramSj = new StringJoiner("&");
            args.forEach((param, value) -> {
                paramSj.add(param + "=" + value);
            });
            final String params = paramSj.toString();
            LOGGER.trace("parameters: {}", params);
            final HttpURLConnection conn = (HttpURLConnection) url
                    .openConnection();
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Accept", "application/json");
            conn.setRequestProperty("Content-Type",
                    "application/x-www-form-urlencoded");
            conn.setRequestProperty("Authorization",
                    "Basic " + getBasicAuthentication());
            final byte[] paramsBytes = params.toString().getBytes("UTF-8");
            conn.setRequestProperty("Content-Length",
                    String.valueOf(paramsBytes.length));
            conn.setDoInput(true);
            conn.setDoOutput(true);
            conn.getOutputStream().write(paramsBytes);

            LOGGER.trace("responseCode={}", conn.getResponseCode());
            if (conn.getResponseCode() != HttpServletResponse.SC_OK) {
                final String responseContent = getResponseContent(
                        conn.getErrorStream());
                conn.disconnect();
                LOGGER.error(responseContent);
                throw new RuntimeException(
                        "Failed : HTTP error code=" + conn.getResponseCode()
                        + ", response=" + responseContent);
            }

            final String responseContent = getResponseContent(
                    conn.getInputStream());
            conn.disconnect();
            return responseContent;

        } catch (final IOException e) {
            LOGGER.catching(e);
        }

        return null;
    }

    /**
     * @param code
     *            Authorization code returned by the server in the query string
     *            after user authentication.
     * @return success
     */
    public final boolean retrieveToken(@NonNull final String code) {
        Objects.requireNonNull(clientId, "clientId must not be null!");
        Objects.requireNonNull(clientSecret, "clientSecret must not be null!");
        final Map<String, String> args = new HashMap<>();
        args.put("grant_type", "authorization_code");
        args.put("code", code);
        args.put("redirect_uri", getRedirectUrl());
        args.put("client_id", encodeBase64(clientId));
        args.put("client_secret", encodeBase64(clientSecret));
        final String json = post(ACCESS_TOKEN_URL, args);
        LOGGER.info("JSON={}", json);
        if (json == null) {
            LOGGER.error("Response is null!");
            return false;
        }
        final String error = extract(json, "error");
        if (error != null) {
            final String errorDescription = extract(json, "error_description");
            LOGGER.trace("error=%s, error_description=%s", error,
                    errorDescription);
            return false;
        }
        accessToken = extract(json, "access_token");
        final String tokenType = extract(json, "token_type");
        // String refreshToken = extract(json, "refresh_token");
        if (!"Bearer".equals(tokenType)) {
            throw new RuntimeException("Token type not handled: " + tokenType);
        }
        return true;
    }
}
