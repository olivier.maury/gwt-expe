package fr.inrae.agroclim.gwtexpe.server.rs;

/*-
 * #%L
 * GWT Experiments
 * %%
 * Copyright (C) 2018 - 2022 INRAE AgroClim
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.ArrayList;
import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpSession;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import fr.inrae.agroclim.gwtexpe.server.ApplicationUtils;
import fr.inrae.agroclim.gwtexpe.server.SessionCollectorListener;
import fr.inrae.agroclim.gwtexpe.server.model.ApplicationInfo;
import io.swagger.v3.oas.annotations.Operation;

/**
 * Application JAX-RS resource.
 */
@Path("application")
@RequestScoped
public class ApplicationResource {

    /**
     * Injected context.
     */
    @javax.ws.rs.core.Context
    private ServletContext servletContext;

    /**
     * Details about the application.
     *
     * @return application info
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("info")
    @Operation(
            summary = "Get application info.",
            description = "Get the name, version and build date of the application."
            )
    public ApplicationInfo info() {
        return ApplicationUtils.getInfo();
    }

    /**
     * List all sessions of connected users.
     *
     * @return user sessions.
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("sessions")
    @Operation(summary = "Get user sessions.", description = "Get the name and id of users using the application.")
    public List<String> sessions() {
        final List<String> list = new ArrayList<>();
        final String accountId = SessionCollectorListener.getAccountIdAttribute();
        List<HttpSession> sessions;
        sessions = SessionCollectorListener.getSessions(servletContext);
        list.add(sessions.size() + " sessions");
        sessions.forEach((session) -> {
            final Object val = session.getAttribute(accountId);
            if (val != null) {
                list.add(val.toString());
            } else {
                list.add("-");
            }
        });
        return list;
    }
}
