package fr.inrae.agroclim.gwtexpe.server.rs.filter;

/*-
 * #%L
 * GWT Experiments
 * %%
 * Copyright (C) 2018 - 2022 INRAE AgroClim
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.io.IOException;

import javax.annotation.Priority;
import javax.ws.rs.Priorities;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.ContainerResponseContext;
import javax.ws.rs.container.ContainerResponseFilter;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.ext.Provider;

/**
 * Filter for JAX-RS resources.
 */
@Priority(Priorities.USER)
@Provider
public class LogFilter
implements ContainerRequestFilter, ContainerResponseFilter {

    @Override
    public final void filter(final ContainerRequestContext reqContext)
            throws IOException {
        System.out.println("-- request info --");
        final UriInfo uriInfo = reqContext.getUriInfo();
        log(uriInfo, reqContext.getHeaders(), null);
    }

    @Override
    public final void filter(final ContainerRequestContext reqContext,
            final ContainerResponseContext resContext) throws IOException {
        System.out.println("-- response info --");
        final UriInfo uriInfo = reqContext.getUriInfo();
        log(uriInfo, reqContext.getHeaders(), resContext.getHeaders());
    }

    /**
     * Log to STD out.
     *
     * @param uriInfo    uri info
     * @param reqHeaders headers of the request
     * @param resHeaders headers of the response
     */
    private void log(final UriInfo uriInfo,
            final MultivaluedMap<String, ?> reqHeaders,
            final MultivaluedMap<String, ?> resHeaders) {
        System.out.println("Path: " + uriInfo.getPath());
        System.out.println("HTTP headers of the request:");
        reqHeaders.forEach((k, v) -> System.out.println(" - " + k + ": " + v));
        if (resHeaders != null) {
            System.out.println("HTTP headers of the response:");
            resHeaders.forEach(
                    (k, v) -> System.out.println(" - " + k + ": " + v));
        }
    }
}
