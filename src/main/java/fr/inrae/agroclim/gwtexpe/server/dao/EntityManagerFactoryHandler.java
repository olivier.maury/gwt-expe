package fr.inrae.agroclim.gwtexpe.server.dao;

/*-
 * #%L
 * GWT Experiments
 * %%
 * Copyright (C) 2018 - 2022 INRAE AgroClim
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;

/**
 * Used by PersistenceManager to create a ScopedEntityManagerFactory.
 */
@RequiredArgsConstructor
@Log4j2
public final class EntityManagerFactoryHandler implements InvocationHandler {

    /**
     * Method of EntityManagerFactory.
     */
    private static final Method CREATE_ENTITY_MANAGER;

    /**
     * Method of ScopedEntityManagerFactory.
     */
    private static final Method CREATE_SCOPED_ENTITY_MANAGER;

    static {
        try {
            CREATE_ENTITY_MANAGER = EntityManagerFactory.class
                    .getMethod("createEntityManager");
            CREATE_SCOPED_ENTITY_MANAGER = ScopedEntityManagerFactory.class
                    .getMethod("createScopedEntityManager");
        } catch (final NoSuchMethodException e) {
            throw new ExceptionInInitializerError(e);
        }
    }

    /**
     * EntityManagerFactory, injected in Constructor.
     */
    private final EntityManagerFactory emf;

    @Override
    public Object invoke(final Object o, final Method method, final Object[] os)
            throws Throwable {
        LOGGER.traceEntry("enter {} {} {}", ScopedEntityManagerFactory.class.getCanonicalName(),
                method.getName(), os);
        Object result;
        try {
            if (method.equals(CREATE_SCOPED_ENTITY_MANAGER)) {
                final EntityManager target = (EntityManager) CREATE_ENTITY_MANAGER
                        .invoke(emf, os);
                result = Proxy.newProxyInstance(
                        ScopedEntityManager.class.getClassLoader(),
                        new Class[] {ScopedEntityManager.class},
                        new EntityManagerHandler(target));
            } else {
                result = method.invoke(emf, os);
            }
        } catch (final IllegalAccessException | IllegalArgumentException
                | InvocationTargetException e) {
            throw LOGGER.throwing(e);
        }
        if (method.getReturnType().getClass().equals(Void.class.getClass())) {
            LOGGER.traceExit("No return");
        } else {
            LOGGER.traceExit(result);
        }
        return result;
    }

}
