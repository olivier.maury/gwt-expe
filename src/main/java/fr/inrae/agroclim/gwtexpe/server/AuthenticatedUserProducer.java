package fr.inrae.agroclim.gwtexpe.server;

/*-
 * #%L
 * GWT Experiments
 * %%
 * Copyright (C) 2018 - 2022 INRAE AgroClim
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import javax.enterprise.context.RequestScoped;
import javax.enterprise.event.Observes;
import javax.enterprise.inject.Produces;
import javax.inject.Inject;

import fr.inrae.agroclim.gwtexpe.server.dao.PersonDao;
import fr.inrae.agroclim.gwtexpe.server.model.Person;
import fr.inrae.agroclim.gwtexpe.server.model.User;
import fr.inrae.agroclim.gwtexpe.server.rs.filter.AuthenticatedUser;
import lombok.extern.log4j.Log4j2;

/**
 * <code>@RequestScoped</code>, allowing you to share data between filters and
 * your beans. The CDI approach allows you to get the authenticated user from
 * beans other than JAX-RS resources and providers.
 */
@RequestScoped
@Log4j2
public final class AuthenticatedUserProducer {
    /**
     * DAO for User.
     */
    @Inject
    private PersonDao personDao;

    /**
     * User instance that can be injected into container managed beans, such as
     * JAX-RS services, CDI beans, servlets and EJBs.
     *
     * Use the following piece of code to inject a User instance (in fact, it's
     * a CDI proxy):
     *
     * <pre>
     * &#64;Inject
     * &#64;AuthenticatedUser
     * User authenticatedUser;
     * </pre>
     */
    @Produces
    @RequestScoped
    @AuthenticatedUser
    private User authenticatedUser;

    /**
     * Hit the the database to find a user by its username and set the user.
     *
     * @param username
     *            user's name
     */
    public void handleAuthenticationEvent(
            final @Observes @AuthenticatedUser String username) {
        LOGGER.traceEntry();
        final Person p = personDao.findBy(username);
        this.authenticatedUser = new User();
        if (p == null) {
            return;
        }
        this.authenticatedUser.setCreated(p.getCreated());
        this.authenticatedUser.setId(p.getId());
        this.authenticatedUser.setName(p.getName());
    }

}
