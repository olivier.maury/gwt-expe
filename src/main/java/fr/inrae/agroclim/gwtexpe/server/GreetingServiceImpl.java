package fr.inrae.agroclim.gwtexpe.server;

/*-
 * #%L
 * GWT Experiments
 * %%
 * Copyright (C) 2018 - 2022 INRAE AgroClim
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.Locale;

import javax.enterprise.context.RequestScoped;
import javax.enterprise.event.Event;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;

import fr.inrae.agroclim.gwtexpe.client.GreetingService;
import fr.inrae.agroclim.gwtexpe.server.dao.PersonDao;
import fr.inrae.agroclim.gwtexpe.server.model.Person;
import fr.inrae.agroclim.gwtexpe.shared.FieldVerifier;
import lombok.extern.log4j.Log4j2;

/**
 * The server-side implementation of the RPC service.
 */
@Log4j2
@RequestScoped
@WebServlet(urlPatterns = { "/gwtexpe/greet" })
public final class GreetingServiceImpl extends RemoteServiceServlet
implements GreetingService {
    /**
     * UUID for Serializable.
     */
    private static final long serialVersionUID = 4079295733539308238L;

    /**
     * HTTP request.
     */
    @Inject
    private HttpServletRequest request;

    /**
     * DAO for Person.
     */
    @Inject
    private PersonDao personDao;

    /**
     * The event raised when the user typed by the user.
     */
    @Inject
    @Named("username")
    private Event<String> usernameEvent;

    /**
     * User representation.
     */
    @Inject
    private You you;

    /**
     * Escape an html string. Escaping data received from the client helps to
     * prevent cross-site script vulnerabilities.
     *
     * @param html
     *            the html string to escape
     * @return the escaped string
     */
    private String escapeHtml(final String html) {
        if (html == null) {
            return null;
        }
        return html.replace("&", "&amp;").replace("<", "&lt;")
                .replace(">", "&gt;");
    }

    @Override
    public String greetServer(final String input)
            throws IllegalArgumentException {
        LOGGER.trace(input);
        // event
        usernameEvent.fire(input);
        // Verify that the input is valid.
        if (!FieldVerifier.isValidName(input)) {
            // If the input is not valid, throw an IllegalArgumentException back
            // to the client.
            throw new IllegalArgumentException(
                    "Name must be at least 4 characters long");
        }

        you.setName(input);
        String known = "not at all";
        if (personDao == null) {
            LOGGER.warn("personDao must not be null!");
        } else {
            final Person person = personDao.findOrCreateBy(input);
            if (person != null) {
                LOGGER.trace("person.id={}", person.getId());
                getThreadLocalRequest().getSession(true).setAttribute(
                        "accountId", person.getId());
                known = person.toString();
            } else {
                LOGGER.trace("person id null");
            }
        }

        final String serverInfo = getServletContext().getServerInfo();
        String userAgent = getThreadLocalRequest().getHeader("User-Agent");

        // Escape data from the client to avoid cross-site script
        // vulnerabilities.
        final String msg = escapeHtml(input);
        userAgent = escapeHtml(userAgent);

        final Locale locale = LocaleUtils.getLocale(request);
        final I18n i18n = new I18n("fr.inrae.agroclim.gwtexpe.server.i18n", locale);

        return i18n.format("hello", msg, known, serverInfo, userAgent);
    }

    @Override
    public String userName() {
        if (you == null) {
            return null;
        }
        return you.getName();
    }
}
