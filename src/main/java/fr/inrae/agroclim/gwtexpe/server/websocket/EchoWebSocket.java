package fr.inrae.agroclim.gwtexpe.server.websocket;

/*-
 * #%L
 * GWT Experiments
 * %%
 * Copyright (C) 2018 - 2022 INRAE AgroClim
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.io.IOException;

import javax.inject.Inject;
import javax.websocket.OnClose;
import javax.websocket.OnError;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;

import lombok.extern.log4j.Log4j2;

/**
 * Websocket end point "/echo".
 */
@ServerEndpoint("/echo")
@Log4j2
public final class EchoWebSocket {
    /**
     * WebSocket session handler.
     */
    @Inject
    private SessionHandler sessionHandler;

    /**
     * Called when a web socket session is closing.
     *
     * @param session
     *            closed session.
     */
    @OnClose
    public void close(final Session session) {
        LOGGER.traceEntry();
        if (getSessionHandler() == null) {
            LOGGER.error("sessionHandler is null!");
        } else {
            getSessionHandler().removeSession(session);
        }
    }

    /**
     * Called to receive incoming web socket messages.
     *
     * @param session
     *            WebSocket session
     * @param msg
     *            the whole message
     */
    @OnMessage
    public void echoTextMessage(final Session session, final String msg) {
        LOGGER.traceEntry(msg);
        String message = msg;
        try {
            if (session.isOpen()) {
                if (getSessionHandler() != null) {
                    message += " (" + getSessionHandler().getSessions().size()
                            + ")";
                }
                session.getBasicRemote().sendText(message);
            }
        } catch (final IOException e) {
            try {
                session.close();
            } catch (final IOException e1) {
                LOGGER.warn("Exception while closing session", e1);
            }
        }
    }

    /**
     * Hack because Inject does not work!
     *
     * @return sessionHandler
     */
    private SessionHandler getSessionHandler() {
        if (sessionHandler == null) {
            sessionHandler = SessionHandler.getInstance();
        }
        return sessionHandler;
    }

    /**
     * Called in order to handle errors.
     *
     * @param error
     *            error
     */
    @OnError
    public void onError(final Throwable error) {
        LOGGER.error(error);
    }

    /**
     * Called when a new web socket session is open.
     *
     * @param session
     *            WebSocket session
     */
    @OnOpen
    public void open(final Session session) {
        LOGGER.traceEntry();
        String msg = "Welcome!";
        if (getSessionHandler() == null) {
            LOGGER.error("sessionHandler is null!");
            msg += " sessionHandler is null!";
        } else {
            getSessionHandler().addSession(session);
        }
        try {
            session.getBasicRemote().sendText(msg);
        } catch (final IOException e) {
            LOGGER.warn("Exception while sending text", e);
        }
    }
}
