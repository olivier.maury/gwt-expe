package fr.inrae.agroclim.gwtexpe.server.ws;

/*-
 * #%L
 * GWT Experiments
 * %%
 * Copyright (C) 2018 - 2022 INRAE AgroClim
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.ArrayList;

import javax.inject.Inject;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;

import fr.inrae.agroclim.gwtexpe.server.ApplicationUtils;
import fr.inrae.agroclim.gwtexpe.server.dao.PersonDao;
import fr.inrae.agroclim.gwtexpe.server.model.ApplicationInfo;
import fr.inrae.agroclim.gwtexpe.server.model.Person;

/**
 * Application JAX-WS resource.
 */
@WebService(targetNamespace = "http://gwtexpe.agroclim.inrae.fr")
@SOAPBinding(style = SOAPBinding.Style.RPC)
public class ApplicationService {
    /**
     * DAO for User.
     */
    @Inject
    private PersonDao personDao;

    /**
     * Details about the application.
     *
     * @return application info
     */
    public ApplicationInfo info() {
        return ApplicationUtils.getInfo();
    }

    /**
     * List all sessions of connected users.
     *
     * @return user sessions.
     */
    public ArrayList<String> names() {
        final ArrayList<String> list = new ArrayList<>();
        if (personDao == null) {
            throw new RuntimeException("Injection failed!");
        } else {
            personDao.findAll().stream().map(Person::getName).forEach(list::add);
        }
        return list;
    }
}
