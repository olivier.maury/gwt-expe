package fr.inrae.agroclim.gwtexpe.server.rs.filter;

/*-
 * #%L
 * GWT Experiments
 * %%
 * Copyright (C) 2018 - 2022 INRAE AgroClim
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.io.IOException;

import javax.annotation.Priority;
import javax.enterprise.inject.spi.BeanManager;
import javax.enterprise.util.AnnotationLiteral;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.ws.rs.Priorities;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.Provider;

import fr.inrae.agroclim.gwtexpe.server.jwt.JsonWebTokenService;
import fr.inrae.agroclim.gwtexpe.server.model.User;
import lombok.extern.log4j.Log4j2;

/**
 * Defined name-binding annotation will be used to decorate a filter class,
 * which implements ContainerRequestFilter, allowing you to intercept the
 * request before it be handled by a resource method. The
 * ContainerRequestContext can be used to access the HTTP request headers and
 * then extract the token.
 */
@Secured
@Provider
@Priority(Priorities.AUTHENTICATION)
@Log4j2
public class AuthenticationFilter implements ContainerRequestFilter {

    /**
     * Event qualifier to fire event when user authenticates.
     */
    private class AuthenticatedUserQualifier extends
    AnnotationLiteral<AuthenticatedUser> {
        /**
         * UID for serializable.
         */
        private static final long serialVersionUID = -7469415892282811742L;
    }

    /**
     * Identifier of protection domain in
     * WWW-Authenticate: Basic realm="REALM".
     */
    private static final String REALM = "example";

    /**
     * Word in HTTP header Authorization: before the token.
     */
    private static final String AUTHENTICATION_SCHEME = "Bearer";

    /*-
     * This does not work...
    @Inject
    @AuthenticatedUser
    private Event<String> userAuthenticatedEvent;
     */

    /**
     * Creation and validation of JSON web tokens.
     */
    private final JsonWebTokenService jwtService = new JsonWebTokenService();

    @Override
    public final void filter(final ContainerRequestContext requestContext)
            throws IOException {
        LOGGER.traceEntry();
        // Get the Authorization header from the request
        final String authorizationHeader = requestContext
                .getHeaderString(HttpHeaders.AUTHORIZATION);

        // Validate the Authorization header
        if (!isTokenBasedAuthentication(authorizationHeader)) {
            abortWithUnauthorized(requestContext);
            return;
        }

        // Extract the token from the Authorization header
        final String token = authorizationHeader
                .substring(AUTHENTICATION_SCHEME.length()).trim();

        // Check if the token was issued by the server and if it's not expired.
        final User user = jwtService.retrieveUser(token);
        if (user == null) {
            abortWithUnauthorized(requestContext);
        } else {
            //  the token is issued for a user
            // and the token will be used to look up the user identifier
            final String username = user.getName();
            System.out.println("-=" + username + "=-");
            // If the authentication succeeds, fire the event passing the
            // username as parameter
            // userAuthenticatedEvent.fire(username);
            lookUpBeanManager().fireEvent(username,
                    new AuthenticatedUserQualifier()
                    );
            System.out.println("-=" + username + "=-");
        }
    }

    /**
     * Check if the Authorization header is valid.
     *
     * It must not be null and must be prefixed with "Bearer" plus a
     * whitespace. The authentication scheme comparison must be
     * case-insensitive.
     *
     * @param authorizationHeader header to check
     * @return the header is an authorization header
     */
    private boolean isTokenBasedAuthentication(
            final String authorizationHeader) {
        return authorizationHeader != null && authorizationHeader.toLowerCase()
                .startsWith(AUTHENTICATION_SCHEME.toLowerCase() + " ");
    }

    /**
     * Abort the filter chain with a 401 status code response.
     * The WWW-Authenticate header is sent along with the response.
     *
     * @param requestContext context to abort
     */
    private void abortWithUnauthorized(
            final ContainerRequestContext requestContext) {
        requestContext.abortWith(Response.status(Response.Status.UNAUTHORIZED)
                .header(HttpHeaders.WWW_AUTHENTICATE,
                        AUTHENTICATION_SCHEME + " realm=\"" + REALM + "\"")
                .build());
    }

    /**
     * Hack instead of using @Inject Event<String> username.
     * @return beanManager
     */
    private BeanManager lookUpBeanManager() {
        try {
            // See reference below about how I came up with this
            final InitialContext iniCtx = new InitialContext();
            BeanManager bm;
            bm = (BeanManager) iniCtx.lookup("java:comp/env/BeanManager");
            return bm;
        } catch (final NamingException e) {
            LOGGER.error("Could not construct BeanManager.", e);
            return null;
        }
    }
}
