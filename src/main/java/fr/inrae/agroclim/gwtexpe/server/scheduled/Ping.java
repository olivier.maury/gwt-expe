package fr.inrae.agroclim.gwtexpe.server.scheduled;

/*-
 * #%L
 * GWT Experiments
 * %%
 * Copyright (C) 2018 - 2022 INRAE AgroClim
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import lombok.extern.log4j.Log4j2;

/**
 * Example of scheduled task : log.
 */
@Log4j2
public class Ping implements Runnable {

    /**
     * String prefix.
     */
    private final String initString;

    /**
     * Start time.
     */
    private final long startTime = System.currentTimeMillis();

    /**
     * Constructor.
     *
     * @param s
     *            string prefix
     */
    public Ping(final String s) {
        this.initString = s;
    }

    @Override
    public final void run() {
        LOGGER.info("{} : {}", initString, startTime);
    }
}
