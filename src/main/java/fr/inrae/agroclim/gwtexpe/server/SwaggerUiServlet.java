package fr.inrae.agroclim.gwtexpe.server;

/*-
 * #%L
 * GWT Experiments
 * %%
 * Copyright (C) 2018 - 2022 INRAE AgroClim
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.io.IOException;
import java.util.ResourceBundle;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Web interface for Swagger UI.
 */
@WebServlet("/swagger-ui")
public class SwaggerUiServlet extends HttpServlet {
    /**
     * serial ID.
     */
    private static final long serialVersionUID = 1228252165929549104L;

    @Override
    public final void doGet(final HttpServletRequest request, final HttpServletResponse response)
            throws ServletException, IOException {
        final ResourceBundle res = ResourceBundle.getBundle("fr.inrae.agroclim.gwtexpe.version");
        request.setAttribute("swagger_version", res.getString("swagger-ui.version"));
        // Draw web page
        final RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/swagger-ui.jsp");
        rd.forward(request, response);
    }
}
