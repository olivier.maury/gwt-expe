package fr.inrae.agroclim.gwtexpe.server.dao;

/*-
 * #%L
 * GWT Experiments
 * %%
 * Copyright (C) 2018 - 2022 INRAE AgroClim
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import javax.persistence.EntityManager;

/**
 * Proxy'ed EntityManager to handle transactions.
 *
 * Used by EntityManagerFactoryHandler, EntityManagerHandler and DAO.
 *
 * EntityManager: This means that a ScopedEntityManager is an extension of an
 * EntityManager offering some functions to manage transactions with the new
 * Java 8 lambda expressions.
 *
 * AutoClosable: This means that with try-with (Java 7) we will not need to
 * worry about closing the EntityManager anymore because Java will do it for us
 * avoiding resource leaks.
 */
public interface ScopedEntityManager extends EntityManager, AutoCloseable {

    /**
     * Operations to execute in transaction.
     */
    @FunctionalInterface
    interface Transaction {
        /**
         * Functional method.
         */
        void execute();
    }

    /**
     * Operations to execute in transaction.
     *
     * @param <T>
     *            returned type.
     */
    @FunctionalInterface
    interface TransactionFunction<T> {
        /**
         * Functional method.
         *
         * @return returned value
         */
        T execute();
    }

    /**
     * To use with try-with.
     *
     * @param t Operations to execute in transaction.
     */
    void executeTransaction(Transaction t);

    /**
     * To use with try-with.
     *
     * @param t Operations to execute in transaction.
     * @return returned value
     * @param <T>
     *            returned type.
     */
    <T> T executeTransaction(TransactionFunction<T> t);
}
