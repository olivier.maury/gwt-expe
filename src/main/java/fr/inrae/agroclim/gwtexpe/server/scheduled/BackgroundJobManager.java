package fr.inrae.agroclim.gwtexpe.server.scheduled;

/*-
 * #%L
 * GWT Experiments
 * %%
 * Copyright (C) 2018 - 2022 INRAE AgroClim
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

import fr.inrae.agroclim.gwtexpe.server.websocket.SessionHandler;
import lombok.extern.log4j.Log4j2;

/**
 * Manager for scheduled tasks.
 */
@WebListener
@Log4j2
public class BackgroundJobManager implements ServletContextListener {

    /**
     * Scheduler for tasks.
     */
    private ScheduledExecutorService scheduler;

    /**
     * WebSocket session handler.
     */
    @Inject
    private SessionHandler sessionHandler;

    @Override
    public final void contextDestroyed(final ServletContextEvent event) {
        scheduler.shutdownNow();
    }

    @Override
    public final void contextInitialized(final ServletContextEvent event) {
        scheduler = Executors.newSingleThreadScheduledExecutor();
        final int seconds = 60;
        scheduler.scheduleAtFixedRate(new Ping(getClass().getSimpleName()), 0,
                seconds, TimeUnit.SECONDS);
        if (sessionHandler == null) {
            LOGGER.error("sessionHandler is null!");
        } else {
            scheduler.scheduleAtFixedRate(
                    new WebSocketSessionTalk(sessionHandler), 0, 1,
                    TimeUnit.MINUTES);
            final int minutes = 5;
            scheduler.scheduleAtFixedRate(() -> sessionHandler.purge(), 0,
                    minutes, TimeUnit.MINUTES);
        }
    }

}
