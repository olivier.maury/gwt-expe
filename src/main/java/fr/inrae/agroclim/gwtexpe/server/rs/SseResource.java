package fr.inrae.agroclim.gwtexpe.server.rs;

/*-
 * #%L
 * GWT Experiments
 * %%
 * Copyright (C) 2018 - 2022 INRAE AgroClim
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.Timer;
import java.util.TimerTask;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Observes;
import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.sse.OutboundSseEvent;
import javax.ws.rs.sse.Sse;
import javax.ws.rs.sse.SseBroadcaster;
import javax.ws.rs.sse.SseEventSink;

import org.glassfish.jersey.media.sse.SseFeature;

import fr.inrae.agroclim.gwtexpe.server.DataTransfert;
import fr.inrae.agroclim.gwtexpe.server.dao.PersonDao;
import fr.inrae.agroclim.gwtexpe.server.model.Person;
import fr.inrae.agroclim.gwtexpe.shared.PersonDTO;
import lombok.extern.log4j.Log4j2;

/**
 * Server-Sent Events (SSE) is an HTTP based specification that provides a way
 * to establish a long-running and mono-channel connection from the server to
 * the client.
 *
 * https://html.spec.whatwg.org/multipage/server-sent-events.html
 */
@ApplicationScoped
@Log4j2
@Path("sse")
public class SseResource {


    /**
     * Maximum event id, to simulate break.
     */
    private static final int MAX_ID = 50;

    /**
     * Reconnect delay for SSE event.
     */
    private static final int RECONNECT_DELAY = 3000;

    /**
     * Builder of single outbound Server-sent event.
     */
    private OutboundSseEvent.Builder eventBuilder;

    /**
     * DAO for User.
     */
    @Inject
    private PersonDao personDao;

    /**
     * Server-Sent events entry point.
     */
    @Context
    private Sse sse;

    /**
     * Server-Sent events broadcaster.
     */
    private SseBroadcaster sseBroadcaster;

    @GET
    @Path("publish")
    public void broadcast() {
        final Timer timer = new Timer();
        final int delay = 0;
        final int timeInterval = 5_000;
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                long lastEventId = 0;
                final Person entity = personDao.findNewerThan(lastEventId);
                if (entity != null) {
                    lastEventId = entity.getId();
                    final OutboundSseEvent sseEvent = eventBuilder
                            .name("message")
                            .id(String.valueOf(lastEventId))
                            .mediaType(MediaType.TEXT_PLAIN_TYPE)
                            .data("New person: " + entity.getName())
                            .reconnectDelay(RECONNECT_DELAY)
                            .comment("new person")
                            .build();
                    sseBroadcaster.broadcast(sseEvent);
                }
                if (lastEventId >= MAX_ID) {
                    timer.cancel();
                }
            }
        }, delay, timeInterval);
    }

    @GET
    @Path("persons")
    @Produces(SseFeature.SERVER_SENT_EVENTS)
    public void getPersons(@Context final SseEventSink sseEventSink,
            @HeaderParam(SseFeature.LAST_EVENT_ID_HEADER) @DefaultValue("-1") final long lastReceivedId) {
        final Timer timer = new Timer();
        final int delay = 0;
        final int timeInterval = 5_000;
        timer.schedule(new TimerTask() {
            private long lastEventId = lastReceivedId;
            @Override
            public void run() {
                final Person entity = personDao.findNewerThan(lastEventId);
                if (entity == null) {
                    return;
                }
                lastEventId = entity.getId();
                final PersonDTO dto = DataTransfert.toDto(entity);
                final OutboundSseEvent sseEvent = eventBuilder
                        .name("person")
                        .id(String.valueOf(lastEventId))
                        .mediaType(MediaType.APPLICATION_JSON_TYPE)
                        .data(PersonDTO.class, dto)
                        .reconnectDelay(RECONNECT_DELAY)
                        .comment("person change")
                        .build();
                sseEventSink.send(sseEvent);
                LOGGER.trace("Person #{} send by SSE", lastEventId);
                // Simulate a while boucle break
                if (lastEventId >= MAX_ID) {
                    timer.cancel();
                    sseEventSink.send(sse.newEvent(">=50 Closing..."));
                    sseEventSink.close();
                }

            }
        }, delay, timeInterval);
    }

    /**
     * Send typed names to SSE /sse/subscribe.
     *
     * @param username typed name.
     */
    public void handleAuthenticationEvent(final @Observes @Named("username") String username) {
        LOGGER.traceEntry(username);
        final PersonDTO dto = new PersonDTO();
        dto.name = username;
        final OutboundSseEvent event = this.eventBuilder
                .name("message")
                .data(PersonDTO.class, dto)
                .mediaType(MediaType.APPLICATION_JSON_TYPE)
                .comment("@AuthenticatedUser")
                .build();
        this.sseBroadcaster.broadcast(event);
        LOGGER.traceExit(username);
    }

    @PostConstruct
    public void init() {
        personDao.findOrCreateBy("Jean");
        personDao.findOrCreateBy("Pierre");
        personDao.findOrCreateBy("Michel");
        personDao.findOrCreateBy("André");
        this.eventBuilder = sse.newEventBuilder();
        this.sseBroadcaster = sse.newBroadcaster();
    }

    @GET
    @Path("subscribe")
    @Produces(SseFeature.SERVER_SENT_EVENTS)
    public void listen(@Context final SseEventSink sseEventSink) {
        sseEventSink.send(sse.newEvent("Welcome !"));
        this.sseBroadcaster.register(sseEventSink);
        sseEventSink.send(sse.newEvent("You are registred !"));
    }

}
