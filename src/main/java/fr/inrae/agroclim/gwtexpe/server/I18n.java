package fr.inrae.agroclim.gwtexpe.server;

/*-
 * #%L
 * GWT Experiments
 * %%
 * Copyright (C) 2018 - 2022 INRAE AgroClim
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.text.MessageFormat;
import java.util.Collections;
import java.util.HashSet;
import java.util.Locale;
import java.util.Objects;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.Set;
import java.util.function.BiFunction;
import java.util.function.BiPredicate;
import java.util.stream.Collectors;

import lombok.Getter;
import lombok.NonNull;

/**
 * Localized messages with plural handling à la GWT.
 *
 * @author Olivier Maury
 */
public class I18n {
    /**
     * Accepted operators, ordered.
     */
    public enum Operator implements BiFunction<Integer, Integer, Boolean> {
        /**
         * Equals.
         */
        AEQ("=", Objects::equals),
        /**
         * Inferior or equals.
         */
        BINFEQ("<=", (a, b) -> a <= b),
        /**
         * Strictly inferior.
         */
        CINF("<", (a, b) -> a < b),
        /**
         * Superior or equals.
         */
        DSUPEQ(">=", (a, b) -> a >= b),
        /**
         * Strictly superior.
         */
        ESUP(">", (a, b) -> a > b);

        /**
         * Guess the right operator in the string comparison.
         *
         * @param string string comparison (eg.: ">=10")
         * @return operator matching the string comparison
         */
        static Optional<Operator> extract(final String string) {
            for (final Operator op : values()) {
                if (string.startsWith(op.symbol)) {
                    return Optional.of(op);
                }
            }
            return Optional.empty();
        }
        /**
         * Comparison function of the operator.
         */
        private final BiPredicate<Integer, Integer> function;
        /**
         * String representation of the operator.
         */
        private final String symbol;
        /**
         * Constructor.
         *
         * @param string String representation of the operator.
         * @param func Comparison function of the operator.
         */
        Operator(final String string,
                final BiPredicate<Integer, Integer> func) {
            symbol = string;
            function = func;
        }

        @Override
        public Boolean apply(final Integer arg0, final Integer arg1) {
            return function.test(arg0, arg1);
        }

        /**
         * @return length of string representation
         */
        public int getLength() {
            return symbol.length();
        }
    }

    /**
     * Bundle name.
     */
    public static final String BUNDLE_NAME = "fr.inrae.agroclim.gwtexpe.server.i18n";

    /**
     * Check if the comparison string matches the value.
     *
     * @param comparison comparison string (eg.: ">10")
     * @param plural value
     * @return if the comparison string matches the value.
     */
    static boolean matches(final String comparison, final int plural) {
        final var operator = Operator.extract(comparison);
        if (operator.isPresent()) {
            final var op = operator.get();
            var val = comparison.substring(op.getLength());
            if (val != null) {
                val = val.trim();
                if (!val.isEmpty()) {
                    final var value = Integer.valueOf(val);
                    return op.apply(plural, value);
                }
            }
        }
        return false;
    }

    /**
     * Fallback resources from .properties file, in case of missing translation.
     */
    private final ResourceBundle fallbackResources;

    /**
     * Translation keys.
     */
    private final Set<String> keys = new HashSet<>();

    /**
     * Requested locale.
     */
    @Getter
    private final Locale locale;

    /**
     * Resources from .properties file.
     */
    private final ResourceBundle resources;

    /**
     * Constructor.
     *
     * @param rb the bundle.
     */
    public I18n(final ResourceBundle rb) {
        resources = rb;
        locale = rb.getLocale();
        final String bundleName = rb.getBaseBundleName();
        if (bundleName != null) {
            fallbackResources = ResourceBundle.getBundle(bundleName, Locale.ROOT);
        } else {
            fallbackResources = null;
        }
    }

    /**
     * Constructor.
     *
     * @param bundleName      Path of .property resource.
     * @param requestedLocale The requested locale for the bundle.
     */
    public I18n(@NonNull final String bundleName, @NonNull final Locale requestedLocale) {
        locale = requestedLocale;
        fallbackResources = ResourceBundle.getBundle(bundleName, Locale.ROOT);
        final ResourceBundle res = ResourceBundle.getBundle(bundleName, locale);
        if (res.getLocale().equals(locale)) {
            resources = res;
        } else {
            resources = fallbackResources;
        }
        keys.addAll(Collections.list(resources.getKeys()));
        keys.addAll(Collections.list(fallbackResources.getKeys()));
    }

    /**
     * Return message with inlined arguments.
     *
     * @param plural value for plural form
     * @param key message key
     * @param messageArguments arguments for the message.
     * @return message with arguments or exclamation message
     */
    public String format(final int plural, final String key,
            final Object... messageArguments) {
        String keyWithSuffix;

        // the suffix for the value
        keyWithSuffix = key + "[=" + plural + "]";
        if (getKeys().contains(keyWithSuffix)) {
            return format(keyWithSuffix, messageArguments);
        }

        // with comparators <, <=, >, >=
        final var suffixes = getKeys().stream()
                .filter(k -> k.startsWith(key + "[") && k.endsWith("]"))
                .map(k -> k.substring(key.length() + 1, k.length() - 1))
                .collect(Collectors.toList());
        for (final String suf : suffixes) {
            if (matches(suf, plural)) {
                keyWithSuffix = key + "[" + suf + "]";
                return format(keyWithSuffix, messageArguments);
            }
        }
        // if not defined, used default
        return format(key, messageArguments);
    }

    /**
     * Return message with inlined arguments.
     *
     * @param key message key
     * @param messageArguments arguments for the message.
     * @return message with arguments or exclamation message
     */
    public String format(final String key, final Object... messageArguments) {
        final String format = this.get(key);
        final MessageFormat messageFormat = new MessageFormat(format, locale);
        return messageFormat.format(messageArguments);
    }

    /**
     * Retrieve message from key.
     *
     * @param key message key
     * @return message value or exclamation message
     */
    public String get(final String key) {
        if (resources.containsKey(key)) {
            return resources.getString(key);
        }
        if (fallbackResources != null && fallbackResources.containsKey(key)) {
            return fallbackResources.getString(key);
        }
        return "!" + key + "!";
    }


    /**
     * @return all keys in the .properties file
     */
    public Set<String> getKeys() {
        return Collections.unmodifiableSet(keys);
    }

}
