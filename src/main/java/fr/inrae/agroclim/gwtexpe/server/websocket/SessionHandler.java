package fr.inrae.agroclim.gwtexpe.server.websocket;

/*-
 * #%L
 * GWT Experiments
 * %%
 * Copyright (C) 2018 - 2022 INRAE AgroClim
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;
import javax.websocket.Session;
import lombok.Getter;
import lombok.extern.log4j.Log4j2;

/**
 * WebSocket session handler.
 */
@ApplicationScoped
@Named
@Log4j2
public class SessionHandler {

    /**
     * Hack because Inject does not work!.
     */
    private static SessionHandler instance;

    /**
     * @return singleton.
     */
    public static SessionHandler getInstance() {
        return instance;
    }

    /**
     * Active WebSocket sessions.
     */
    @Getter
    private final Set<Session> sessions = new HashSet<>();

    /**
     * Constructor.
     */
    public SessionHandler() {
        LOGGER.traceEntry();
        if (instance == null) {
            instance = this;
        }
    }

    /**
     * Add an active session.
     *
     * @param session
     *            active session
     */
    public void addSession(final Session session) {
        sessions.add(session);
    }

    /**
     * Remove closed sessions.
     */
    public void purge() {
        LOGGER.traceEntry();
        Set<Session> closed = new HashSet<>();
        synchronized (sessions) {
            for (Session session : sessions) {
                if (!session.isOpen()) {
                    closed.add(session);
                }
            }
            LOGGER.trace("Removing {} closed sessions", closed.size());
            sessions.removeAll(closed);
        }
        LOGGER.traceExit();
    }

    /**
     * Remove a closed session.
     *
     * @param session
     *            closed session
     */
    public void removeSession(final Session session) {
        sessions.remove(session);
    }

    /**
     * Send a message to active sessions.
     *
     * @param message
     *            message to send
     */
    public void sendToAllConnectedSessions(final String message) {
        LOGGER.traceEntry("{} sessions", sessions.size());
        synchronized (sessions) {
            // Loop through and send to all listeners
            for (final Session session : sessions) {
                // Make sure it's still open
                if (session.isOpen()) {
                    sendToSession(session, message);
                }
            }
        }
    }

    /**
     * Send a message to a session, if failure remove session from active
     * sessions.
     *
     * @param session
     *            receipient session
     * @param message
     *            message to send
     */
    private void sendToSession(final Session session, final String message) {
        try {
            session.getBasicRemote().sendText(message);
        } catch (final IOException ex) {
            sessions.remove(session);
            LOGGER.warn(ex);
        }
    }
}
