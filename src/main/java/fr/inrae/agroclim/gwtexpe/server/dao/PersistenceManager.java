package fr.inrae.agroclim.gwtexpe.server.dao;

/*-
 * #%L
 * GWT Experiments
 * %%
 * Copyright (C) 2018 - 2022 INRAE AgroClim
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.lang.reflect.Proxy;
import java.util.Objects;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 * Tomcat will not offer many features that are out of the box using other
 * Application Servers, so no @PersistenceContext Injection.
 *
 * The creation of the EntityManagerFactory is very expensive and we have to be
 * very careful where and when we are going to create one.
 */
public enum PersistenceManager {

    /**
     * Singleton.
     */
    SINGLETON;

    /**
     * @return singleton
     */
    public static PersistenceManager getInstance() {
        return SINGLETON;
    }

    /**
     * ScopedEntityManagerFactory.
     */
    private ScopedEntityManagerFactory semf;

    /**
     * Close.
     */
    public void closeEntityManagerFactory() {
        if (semf != null) {
            semf.close();
            semf = null;
        }
    }

    /**
     * Shortcut to ScopedEntityManagerFactory.createScopedEntityManager().
     *
     * @return scoped entitymanager to use in try-with
     */
    public ScopedEntityManager createScopedEntityManager() {
        return getScopedEntityManagerFactory().createScopedEntityManager();
    }

    /**
     * Uses the java.reflection.Proxy of Java 8.
     *
     * In this way we are simulating what a JEE compliant server does during the
     * injection and the results in terms of performance are great.
     */
    private void createScopedEntityManagerFactory() {
        final EntityManagerFactory emf = Persistence
                .createEntityManagerFactory("psunit");
        Objects.requireNonNull(emf, "EntityManagerFactory must be found for psunit!");
        semf = (ScopedEntityManagerFactory) Proxy.newProxyInstance(
                ScopedEntityManagerFactory.class.getClassLoader(),
                new Class[] {ScopedEntityManagerFactory.class},
                new EntityManagerFactoryHandler(emf));
    }

    /**
     * Get or create ScopedEntityManagerFactory.
     *
     * @return existing or created ScopedEntityManagerFactory
     */
    private ScopedEntityManagerFactory getScopedEntityManagerFactory() {
        if (semf == null) {
            createScopedEntityManagerFactory();
        }
        return semf;
    }

}
