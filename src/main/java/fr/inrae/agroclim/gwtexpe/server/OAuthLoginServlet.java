package fr.inrae.agroclim.gwtexpe.server;

/*-
 * #%L
 * GWT Experiments
 * %%
 * Copyright (C) 2018 - 2022 INRAE AgroClim
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.io.IOException;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import fr.inrae.agroclim.gwtexpe.server.Configuration.Key;
import lombok.extern.log4j.Log4j2;

/**
 * Servlet to redirect user on OAuth2 login page.
 */
@RequestScoped
@WebServlet("/oauth2/login")
@Log4j2
public class OAuthLoginServlet extends HttpServlet {
    /**
     * Size of random string for state parameter.
     */
    private static final int RANDOM_STRING_SIZE = 12;

    /**
     * serial ID.
     */
    private static final long serialVersionUID = 1228252165929549101L;

    /**
     * Application configuration from context.xml.
     */
    @Inject
    private Configuration config;

    @Override
    public final void doGet(final HttpServletRequest request,
            final HttpServletResponse response) throws IOException {
        LOGGER.traceEntry();
        final OAuthB2Access b2access = new OAuthB2Access();
        b2access.setAppUrl(config.get(Key.APP_URL));
        b2access.setServer(OAuthB2Access.Server.valueOf(config.get(Key.B2ACCESS_SERVER)));
        b2access.setClientId(config.get(Key.B2ACCESS_CLIENT_ID));
        b2access.setClientId(config.get(Key.B2ACCESS_CLIENT_SECRET));
        storeState(request, b2access.makeRandomString(RANDOM_STRING_SIZE));
        final String url = b2access.getAuthorizeUrl();
        LOGGER.trace("Redirect to {}", url);
        response.sendRedirect(url);
    }

    /**
     * Store random string into session.
     *
     * @param request
     *            HTTP request to store in session
     * @param randomString
     *            string to store
     */
    private void storeState(final HttpServletRequest request,
            final String randomString) {
        final HttpSession session = request.getSession(true);
        session.setAttribute("oauth-state", randomString);
    }
}
