package fr.inrae.agroclim.gwtexpe.server;

/*-
 * #%L
 * GWT Experiments
 * %%
 * Copyright (C) 2018 - 2022 INRAE AgroClim
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Locale;
import java.util.StringJoiner;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import lombok.extern.log4j.Log4j2;

/**
 * Servlet to handle uploaded file.
 */
@MultipartConfig(fileSizeThreshold = 1024 * 1024, maxFileSize = 1024 * 1024 * 5, maxRequestSize = 1024 * 1024 * 5 * 5)
@WebServlet("/upload")
@Log4j2
public class UploadServlet extends HttpServlet {
    /**
     * serial ID.
     */
    private static final long serialVersionUID = 1228252165929549103L;

    @Override
    public final void doGet(final HttpServletRequest request, final HttpServletResponse response)
            throws ServletException, IOException {
        final Locale locale = request.getLocale();
        request.setAttribute("lang", locale.getLanguage());
        // Draw web page
        final RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/upload.jsp");
        rd.forward(request, response);
    }

    @Override
    public final void doPost(final HttpServletRequest request, final HttpServletResponse response)
            throws ServletException, IOException {
        LOGGER.traceEntry();
        final StringJoiner sj = new StringJoiner("\n");
        try {
            for (final Part part : request.getParts()) {
                sj.add(part.getName() + " : " + part.getSubmittedFileName());
            }
        } catch (final FileNotFoundException e) {
            LOGGER.error("File not found: {}", e);
            sj.add("File not found: " + e.getMessage());
        } catch (final IllegalStateException e) {
            LOGGER.error("There was an error: {}", e);
            sj.add("There was an error: " + e.getMessage());
        } catch (final IOException e) {
            LOGGER.error("There was an error: {}", e);
            sj.add("There was an error: " + e.getMessage());
        }
        response.setContentType("text/plain");
        response.getWriter().append(sj.toString());
    }
}
