package fr.inrae.agroclim.gwtexpe.server;

/*-
 * #%L
 * GWT Experiments
 * %%
 * Copyright (C) 2018 - 2022 INRAE AgroClim
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.EnumMap;
import java.util.Map;
import java.util.Objects;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.servlet.ServletContext;

import lombok.Getter;
import lombok.NonNull;
import lombok.extern.log4j.Log4j2;

/**
 * Load configuration from context.xml.
 *
 * Last changed : $Date: 2022-03-28 16:55:07 +0200 (lun., 28 mars 2022) $
 *
 * @author Olivier Maury
 */
@ApplicationScoped
@Log4j2
public class Configuration {
    /**
     * Keys for context.xml.
     */
    public enum Key {
        /**
         * The application URL.
         */
        APP_URL("app.url"),
        /**
         * OAuth2 client ID at B2Access.
         */
        B2ACCESS_CLIENT_ID("b2access.client.id"),
        /**
         * Instance of B2Access.
         */
        B2ACCESS_SERVER("b2access.server"),
        /**
         * OAuth2 client secret at B2Access.
         */
        B2ACCESS_CLIENT_SECRET("b2access.client.secret");
        /**
         * Key in config.properties.
         */
        @Getter
        private final String key;

        /**
         * Constructor.
         *
         * @param k key
         */
        Key(final String k) {
            key = k;
        }
    }

    /**
     * Key prefix for the configuration in context.xml.
     */
    private static final String PREFIX = "gwtexpe.";

    /**
     * context.xml to get path of key values.
     */
    @Inject
    private ServletContext servletContext;
    /**
     * Values from context.xml.
     */
    private final Map<Key, String> values = new EnumMap<>(Key.class);

    /**
     * Return value of context.xml.
     *
     * @param key key
     * @return value
     */
    public String get(@NonNull final Key key) {
        LOGGER.traceEntry(key.name());
        return values.get(key);
    }

    /**
     * Initialize configuration from context.xml.
     */
    @PostConstruct
    public void init() {
        LOGGER.traceEntry();
        Objects.requireNonNull(servletContext, "servletContext must not be null!");
        LOGGER.trace("Loading from context.xml");
        for (final Key key : Key.values()) {
            final String strKey = PREFIX + key.getKey();
            final String value = servletContext.getInitParameter(strKey);
            Objects.requireNonNull(value, "Key " + strKey + " must have value in context.xml");
            values.put(key, value);
        }
    }

}
