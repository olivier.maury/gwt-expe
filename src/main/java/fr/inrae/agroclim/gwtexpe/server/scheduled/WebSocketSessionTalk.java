package fr.inrae.agroclim.gwtexpe.server.scheduled;

/*-
 * #%L
 * GWT Experiments
 * %%
 * Copyright (C) 2018 - 2022 INRAE AgroClim
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.time.LocalDateTime;

import fr.inrae.agroclim.gwtexpe.server.websocket.SessionHandler;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;

/**
 * Send time to WebSocket connected users.
 */
@Log4j2
@RequiredArgsConstructor
public class WebSocketSessionTalk implements Runnable {

    /**
     * Handler of WebSocket sessions.
     */
    @NonNull
    private SessionHandler sessionHandler;

    @Override
    public final void run() {
        LOGGER.traceEntry();
        sessionHandler
                .sendToAllConnectedSessions(LocalDateTime.now().toString());
        LOGGER.traceExit();
    }
}
