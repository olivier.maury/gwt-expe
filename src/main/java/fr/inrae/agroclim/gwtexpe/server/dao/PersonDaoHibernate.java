package fr.inrae.agroclim.gwtexpe.server.dao;

/*-
 * #%L
 * GWT Experiments
 * %%
 * Copyright (C) 2018 - 2022 INRAE AgroClim
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.List;
import java.util.Objects;

import javax.enterprise.context.ApplicationScoped;
import javax.persistence.TypedQuery;

import fr.inrae.agroclim.gwtexpe.server.model.Person;
import lombok.extern.log4j.Log4j2;

/**
 * DAO for Person with Hibernate.
 */
@Log4j2
@ApplicationScoped
public final class PersonDaoHibernate extends DaoHibernate implements PersonDao {

    /**
     * Persist a person.
     *
     * @param person
     *            person to persist
     */
    private void create(final Person person) {
        LOGGER.traceEntry();
        try (ScopedEntityManager em = getScopedEntityManager()) {
            em.executeTransaction(() -> em.persist(person));
        }
        LOGGER.traceExit();
    }
    @Override
    public List<Person> findAll() {
        LOGGER.traceEntry();
        try (ScopedEntityManager em = getScopedEntityManager()) {
            final TypedQuery<Person> query = em.createQuery(
                    "SELECT p FROM Person p", Person.class);
            Objects.requireNonNull(query, "query must not be null!");
            LOGGER.traceExit();
            return query.getResultList();
        } catch (final javax.persistence.NoResultException e) {
            LOGGER.traceExit();
            return null;
        }
    }

    @Override
    public Person findBy(final String name) {
        LOGGER.traceEntry();
        try (ScopedEntityManager em = getScopedEntityManager()) {
            final TypedQuery<Person> query = em.createQuery(
                    "SELECT p FROM Person p WHERE p.name=:name", Person.class);
            query.setParameter("name", name);
            LOGGER.traceExit();
            return query.getSingleResult();
        } catch (final javax.persistence.NoResultException e) {
            LOGGER.traceExit();
            return null;
        }
    }

    @Override
    public Person findNewerThan(final long id) {
        LOGGER.traceEntry();
        try (ScopedEntityManager em = getScopedEntityManager()) {
            final TypedQuery<Person> query = em.createQuery(
                    "SELECT p FROM Person p WHERE id>:id ORDER BY id", Person.class);
            Objects.requireNonNull(query, "query must not be null!");
            query.setParameter("id", id);
            query.setMaxResults(1);
            LOGGER.traceExit();
            return query.getSingleResult();
        } catch (final javax.persistence.NoResultException e) {
            LOGGER.traceExit();
            return null;
        }
    }
    @Override
    public Person findOrCreateBy(final String name) {
        LOGGER.traceEntry();
        try (ScopedEntityManager em = getScopedEntityManager()) {
            final TypedQuery<Person> query = em.createQuery(
                    "SELECT p FROM Person p WHERE name=:name", Person.class);
            query.setParameter("name", name);
            LOGGER.traceExit();
            return query.getSingleResult();
        } catch (final javax.persistence.NoResultException e) {
            final Person person = new Person();
            person.setName(name);
            create(person);
            LOGGER.traceExit();
            return person;
        }
    }

}
