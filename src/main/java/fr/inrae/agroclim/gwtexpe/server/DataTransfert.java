package fr.inrae.agroclim.gwtexpe.server;

/*-
 * #%L
 * GWT Experiments
 * %%
 * Copyright (C) 2018 - 2022 INRAE AgroClim
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.time.ZoneId;

import fr.inrae.agroclim.gwtexpe.server.model.Person;
import fr.inrae.agroclim.gwtexpe.shared.PersonDTO;

public abstract class DataTransfert {

    /**
     * Coeff to convert second to milliseconds.
     */
    private static final int SEC_TO_MS = 1000;

    public static PersonDTO toDto(final Person entity) {
        final ZoneId zoneId = ZoneId.of("GMT");
        final PersonDTO dto = new PersonDTO();
        dto.created = entity.getCreated().atZone(zoneId).toEpochSecond() * SEC_TO_MS;
        dto.id = entity.getId();
        dto.name = entity.getName();
        return dto;
    }

}
