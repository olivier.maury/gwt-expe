package fr.inrae.agroclim.gwtexpe.server;

/*-
 * #%L
 * GWT Experiments
 * %%
 * Copyright (C) 2018 - 2022 INRAE AgroClim
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.Locale;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

/**
 * Locale utils for servlet and server side service.
 *
 * @author Olivier Maury
 */
public interface LocaleUtils {
    /**
     * @return The default language for the application: English.
     */
    static Locale getDefaultLocale() {
        return Locale.ENGLISH;
    }

    /**
     * Guess the allowed locale according to URL parameter, cookie and request.
     *
     * @param request an {@link HttpServletRequest} object that contains the request
     *                the client has made of the servlet
     * @return the locale from the user request
     */
    static Locale getLocale(final HttpServletRequest request) {
        if (request == null) {
            return getDefaultLocale();
        }
        // 1. URL parameter
        final String localeParam = request.getParameter("locale");
        if (localeParam != null) {
            final var found = getLocale(localeParam);
            if (found != null) {
                return found;
            }
        }
        // 2. HTTP header
        if (request.getLocale() == null) {
            return getDefaultLocale();
        }
        if (Locale.FRANCE == request.getLocale()) {
            return Locale.FRENCH;
        }
        if (getLocales().contains(request.getLocale())) {
            return request.getLocale();
        }
        return getDefaultLocale();
    }

    /**
     * Guess the allowed locale according to the language tag.
     *
     * @param tag language tag
     * @return found allowed language or null
     */
    static Locale getLocale(final String tag) {
        if (tag.startsWith("fr")) {
            return Locale.FRENCH;
        }
        final var found = Locale.forLanguageTag(tag);
        if (found == null || !getLocales().contains(found)) {
            return Locale.ENGLISH;
        }
        return found;
    }

    /**
     * @return supported locales.
     */
    static Set<Locale> getLocales() {
        return Set.of(getDefaultLocale(), Locale.FRENCH);
    }

}
