package fr.inrae.agroclim.gwtexpe.shared;

/*-
 * #%L
 * GWT Experiments
 * %%
 * Copyright (C) 2018 - 2022 INRAE AgroClim
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.Date;

import javax.annotation.Nullable;

import com.fasterxml.jackson.annotation.JsonIgnore;

import jsinterop.annotations.JsIgnore;
import jsinterop.annotations.JsOverlay;
import jsinterop.annotations.JsPackage;
import jsinterop.annotations.JsType;

/**
 * DTO for SSE example.
 */
@JsType(namespace = JsPackage.GLOBAL, name = "Object", isNative = true)
public class PersonDTO {
    /**
     * Creation date (milliseconds since January 1, 1970, 00:00:00 GMT).
     */
    public long created;

    /**
     * ID.
     */
    public long id;

    /**
     * Person's name.
     */
    public String name;

    @JsonIgnore
    @JsOverlay
    @JsIgnore
    public final Date getCreatedDate() {
        if (created == 0) {
            return null;
        } else {
            return new Date(created);
        }
    }

    @JsOverlay
    @JsIgnore
    public final void setCreatedDate(@Nullable final Date date) {
        if (date == null) {
            created = 0;
        } else {
            created = date.getTime();
        }
    }

}
