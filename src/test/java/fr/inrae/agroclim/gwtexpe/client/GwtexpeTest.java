package fr.inrae.agroclim.gwtexpe.client;

/*-
 * #%L
 * GWT Experiments
 * %%
 * Copyright (C) 2018 - 2022 INRAE AgroClim
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.gwt.core.client.GWT;
import com.google.gwt.junit.client.GWTTestCase;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.rpc.ServiceDefTarget;

import fr.inrae.agroclim.gwtexpe.shared.FieldVerifier;

/**
 * GWT JUnit tests must extend GWTTestCase.
 */
public final class GwtexpeTest extends GWTTestCase {

    @Override
    public String getModuleName() {
        return "fr.inrae.agroclim.gwtexpe.GwtexpeJUnit";
    }

    /**
     * Tests the FieldVerifier.
     */
    public void testFieldVerifier() {
        assertFalse(FieldVerifier.isValidName(null));
        assertFalse(FieldVerifier.isValidName(""));
        assertFalse(FieldVerifier.isValidName("a"));
        assertFalse(FieldVerifier.isValidName("ab"));
        assertFalse(FieldVerifier.isValidName("abc"));
        assertTrue(FieldVerifier.isValidName("abcd"));
    }

    /**
     * This test will send a request to the server using the greetServer method
     * in GreetingService and verify the response.
     */
    public void testGreetingService() {
        // Create the service that we will test.
        final GreetingServiceAsync greetingService = GWT
                .create(GreetingService.class);
        final ServiceDefTarget target = (ServiceDefTarget) greetingService;
        target.setServiceEntryPoint(GWT.getModuleBaseURL() + "gwtexpe/greet");

        // Since RPC calls are asynchronous, we will need to wait for a response
        // after this test method returns. This line tells the test runner to
        // wait
        // up to 10 seconds before timing out.
        final int timeoutMillis = 10_000;
        delayTestFinish(timeoutMillis);

        // Send a request to the server.
        greetingService.greetServer("GWT User", new AsyncCallback<String>() {
            @Override
            public void onFailure(final Throwable caught) {
                // The request resulted in an unexpected error.
                fail("Request failure: " + caught.getMessage());
            }

            @Override
            public void onSuccess(final String result) {
                // Verify that the response is correct.
                assertTrue(result.startsWith("Hello, GWT User!"));

                // Now that we have received a response, we need to tell the
                // test runner
                // that the test is complete. You must call finishTest() after
                // an
                // asynchronous test finishes successfully, or the test will
                // time out.
                finishTest();
            }
        });
    }

}
