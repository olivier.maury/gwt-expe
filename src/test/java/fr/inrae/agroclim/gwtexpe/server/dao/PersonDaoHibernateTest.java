package fr.inrae.agroclim.gwtexpe.server.dao;

/*-
 * #%L
 * GWT Experiments
 * %%
 * Copyright (C) 2018 - 2022 INRAE AgroClim
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.util.List;

import org.junit.jupiter.api.Test;

import fr.inrae.agroclim.gwtexpe.server.model.Person;

/**
 * Test DAO.
 */
class PersonDaoHibernateTest {

    /**
     * DAO to test.
     */
    private final PersonDaoHibernate dao = new PersonDaoHibernate();

    @Test
    void create() {
        final String name = "toto";
        List<Person> found;
        found = dao.findAll();
        assertNotNull(found);
        assertEquals(0, found.size(), "No data at start");
        dao.findOrCreateBy(name);
        found = dao.findAll();
        assertNotNull(found, "Data after creation");
        assertEquals(1, found.size(), "One after creation");
        assertEquals(name, found.get(0).getName());
    }
}
