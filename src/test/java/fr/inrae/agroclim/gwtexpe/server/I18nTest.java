package fr.inrae.agroclim.gwtexpe.server;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.stream.Stream;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

/**
 * Test plural form handling with I18n.
 */
class I18nTest {


    /**
     * Key for plural messages to test.
     */
    private static final String KEY = "cartItems";
    /**
     * Name of test bundle.
     */
    private static final String NAME = "fr.inrae.agroclim.gwtexpe.server.test";

    /**
     * {@link MethodSource} annotation marks this method as parameters provider.
     *
     * @return combinaisons of nb, key, expected translation
     */
    static Stream<Arguments> formatPluralData() {
        return Stream.of(//
                Arguments.of(1, KEY, "Il y a un produit dans votre panier."), //
                Arguments.of(2, KEY, "Il y a deux produits dans votre panier."), //
                Arguments.of(3, KEY, "Il y a 3 produits dans votre panier, ce qui est peu."), //
                Arguments.of(42, KEY, "Il y a 42 produits dans votre panier, ce qui est un nombre spécial."), //
                Arguments.of(101, KEY, "Il y a 101 produits dans votre panier, ce qui est beaucoup."), //
                Arguments.of(1_314, KEY, "Il y a 1 314 produits dans votre panier, ce qui est beaucoup."), //
                Arguments.of(1, "cats", "Un chat"), //
                // Variation is not present, default value.
                Arguments.of(2, "cats", "2 chats"), //
                Arguments.of(1, "comparison", "Valeur == 1"), //
                Arguments.of(10, "comparison", "Valeur >= 10"), //
                Arguments.of(11, "comparison", "Valeur > 10"), //
                Arguments.of(-10, "comparison", "Valeur <= -10"), //
                Arguments.of(-101, "comparison", "Valeur < -100") //
                );
    }

    /**
     * French translations.
     */
    private final I18n res = new I18n(NAME, Locale.FRENCH);

    @Test
    void contructorWithResourceBundle() {
        final var bundle = ResourceBundle.getBundle(NAME, Locale.FRENCH);
        final var i18n = new I18n(bundle);
        String actual;
        String expected;
        actual = i18n.get("error.title");
        expected = "Erreur";
        assertEquals(expected, actual);

        actual = i18n.get("not.translated");
        expected = "This message is not translated.";
        assertEquals(expected, actual);
    }

    @Test
    void format() {
        final var actual = res.format("warning.missing", "climat", 1, "WIND");
        final var expected = "climat : ligne 1 : WIND manque.";
        assertEquals(expected, actual);
    }

    @ParameterizedTest(name = "formatPluralDefault {index}: {0} {1}")
    @MethodSource("formatPluralData")
    void formatPluralDefault(final int nb, final String key, final String expected) {
        final var actual = res.format(nb, key, nb);
        assertEquals(expected, actual);
    }

    @Test
    void formatPluralSpecialDefaultProperties() {
        final int nb = 42;
        final var korea = new I18n(NAME, Locale.KOREA);
        final var actual = korea.format(nb, KEY, nb);
        final var expected = "There are 42 items in your cart, a special number!";
        assertEquals(expected, actual);
    }

    @Test
    void getStringDefault() {
        final var actual = res.get("not.translated");
        final var expected = "This message is not translated.";
        assertEquals(expected, actual);
    }

    @Test
    void getStringExisting() {
        final var actual = res.get("error.title");
        final var expected = "Erreur";
        assertEquals(expected, actual);
    }

    @Test
    void matches() {
        final Map<String, Integer> doesMatch = new HashMap<>();
        doesMatch.put("=1", 1);
        doesMatch.put(">0", 1);
        doesMatch.put(">=0", 1);
        doesMatch.put(">=1", 1);
        doesMatch.put("<0", -1);
        doesMatch.put("<=0", -1);
        doesMatch.put("<=1", -1);
        doesMatch.put("<-100", -101);
        doesMatch.forEach((comparison, plural) -> {
            final boolean actual = I18n.matches(comparison, plural);
            assertTrue(actual, comparison + " must be true for " + plural);
        });
        final Map<String, Integer> dontMatch = new HashMap<>();
        dontMatch.put("=0", 1);
        dontMatch.put(">1", 1);
        dontMatch.put(">2", 1);
        dontMatch.put(">=2", 1);
        dontMatch.put("<-1", -1);
        dontMatch.put("<-2", -1);
        dontMatch.put("<=-2", -1);
        dontMatch.forEach((comparison, plural) -> {
            final boolean actual = I18n.matches(comparison, plural);
            assertFalse(actual, comparison + " must be false for " + plural);
        });
    }

    @Test
    void operatorExtract() {
        final Map<String, I18n.Operator> operators = new HashMap<>();
        operators.put("1", null);
        operators.put("=1", I18n.Operator.AEQ);
        operators.put(">0", I18n.Operator.ESUP);
        operators.put(">=0", I18n.Operator.DSUPEQ);
        operators.put(">=1", I18n.Operator.DSUPEQ);
        operators.put("<0", I18n.Operator.CINF);
        operators.put("<=0", I18n.Operator.BINFEQ);
        operators.forEach((comparison, expected) -> {
            final I18n.Operator actual = I18n.Operator.extract(comparison) //
                    .orElse(null);
            assertEquals(expected, actual, expected + " must be extracted from " + comparison);
        });
    }
}
