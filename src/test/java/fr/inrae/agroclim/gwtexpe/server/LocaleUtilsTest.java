package fr.inrae.agroclim.gwtexpe.server;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import java.util.Locale;

import javax.servlet.http.HttpServletRequest;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

/**
 * JUnit test of {@link LocaleUtils}.
 *
 * @author Olivier Maury
 */
@ExtendWith(MockitoExtension.class)
class LocaleUtilsTest {

    /**
     * HTTP servlet request with "locale" parameter.
     */
    private final HttpServletRequest request;

    LocaleUtilsTest(@Mock final HttpServletRequest mockedRequest) {
        this.request = mockedRequest;
    }

    @Test
    void getLocaleDefault() {
        when(request.getParameter("locale")).thenReturn(null);
        final Locale actual = LocaleUtils.getLocale(request);
        final Locale expected = LocaleUtils.getDefaultLocale();
        assertEquals(expected, actual);
    }

    @Test
    void getLocaleEnParam() {
        when(request.getParameter("locale")).thenReturn("en");
        final Locale actual = LocaleUtils.getLocale(request);
        final Locale expected = Locale.ENGLISH;
        assertEquals(expected, actual);
    }

    @Test
    void getLocaleEoParam() {
        when(request.getParameter("locale")).thenReturn("eo");
        final Locale actual = LocaleUtils.getLocale(request);
        final Locale expected = LocaleUtils.getDefaultLocale();
        assertEquals(expected, actual);
    }

    @Test
    void getLocaleFrFR() {
        final Locale actual = LocaleUtils.getLocale(Locale.FRANCE.toLanguageTag());
        final Locale expected = Locale.FRENCH;
        assertEquals(expected, actual);
    }

    @Test
    void getLocaleFrFRLocale() {
        when(request.getLocale()).thenReturn(Locale.FRANCE);
        final Locale actual = LocaleUtils.getLocale(request);
        final Locale expected = Locale.FRENCH;
        assertEquals(expected, actual);
    }

    @Test
    void getLocaleFrFRParam() {
        when(request.getParameter("locale")).thenReturn("fr_FR");
        final Locale actual = LocaleUtils.getLocale(request);
        final Locale expected = Locale.FRENCH;
        assertEquals(expected, actual);
    }

    @Test
    void getLocaleFrParam() {
        when(request.getParameter("locale")).thenReturn("fr");
        final Locale actual = LocaleUtils.getLocale(request);
        final Locale expected = Locale.FRENCH;
        assertEquals(expected, actual);
    }

}
