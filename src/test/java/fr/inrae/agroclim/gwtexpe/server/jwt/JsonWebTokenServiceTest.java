package fr.inrae.agroclim.gwtexpe.server.jwt;

/*-
 * #%L
 * GWT Experiments
 * %%
 * Copyright (C) 2018 - 2022 INRAE AgroClim
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.io.UnsupportedEncodingException;

import org.junit.jupiter.api.Test;

import fr.inrae.agroclim.gwtexpe.server.model.User;

/**
 * Test JsonWebTokenService.
 */
class JsonWebTokenServiceTest {

    /**
     * Service to test.
     */
    private final JsonWebTokenService jwt = new JsonWebTokenService();

    /**
     * User to generate token.
     */
    private final User user;

    /**
     * Constructor.
     */
    JsonWebTokenServiceTest() {
        user = new User();
        user.setName("admin");
    }

    /**
     * Test generateToken() and retrieveUser().
     *
     * @throws IllegalArgumentException
     *             raised by service
     * @throws UnsupportedEncodingException
     *             raised by service
     */
    @Test
    public void generateTokenThenRetrieveUser()
            throws IllegalArgumentException, UnsupportedEncodingException {
        assertNotNull(user.getName());
        final String token = jwt.generateToken(user);
        assertNotNull(token);
        final User actual = jwt.retrieveUser(token);
        assertNotNull(actual);
        assertEquals(user.getName(), actual.getName());
    }
}
